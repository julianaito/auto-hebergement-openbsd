





C'est de plus en plus facile de virtualiser un système dans OpenBSD grâce à **vmd**.

	Euh, ok, mais ça veut dire quoi virtualiser ?

Pour faire fonctionner plusieurs systèmes d'exploitation, vous imaginez peut-être qu'il est nécessaire d'avoir plusieurs ordinateurs. Que nenni. Vous pouvez héberger des **systèmes virtuels** sur votre serveur.

Plus simplement, il vous est possible d'avoir plusieurs OpenBSD dans une OpenBSD (openbsdception ^^), comme s'il s'agissait de plusieurs machines indépendantes.

Cela présente de nombreux avantages : 

- C'est facile de séparer plusieurs systèmes et donc de s'organiser.
- Chaque système est indépendant. Si l'un est compromis, les autres normalement non. Encore mieux qu'un chroot.
- Sauvegarder une machine virtuelle est très facile : c'est une simple copie.
- C'est utile pour tester.


Toutefois, cela consomme davantage de ressources.


Avant d'aller plus loin, afin de bien nous comprendre, il faut préciser à quoi fera référence le vocabulaire suivant :

- L'**hôte** fait référence à votre serveur. C'est l'ordinateur qui héberge les machines **virtuelles**. 
- Une **machine virtuelle** est un système d'exploitation. L'**hôte** émule le matériel de cet ordinateur **virtuel**. On l'appellera quelques fois **machine cliente**.


OpenBSD nous propose trois outils pour virtualiser un système : 

- ``vmd`` : le démon qui prend en charge les machines virtuelles. Il permet d'en gérer plusieurs en même temps.
- ``vmctl`` : Permet de gérer une machine virtuelle à la fois : en lancer, en redémarrer, se connecter à la console...
- ``vmm`` : Cet outil permet de surveiller les machines virtuelles.


Avant d'aller plus loin, vérifiez si votre matériel prend en charge la virtualisation avec la commande suivante : 

```
$ dmesg | egrep '(VMX/EPT|SVM/RVI)'
```


Si le résultat n'est pas vide, c'est tout bon :).


N'oubliez pas de mettre à jour les firmwares si besoin : ``# fw_update``.


Activez ``vmd`` avant de passer à la suite :

```
# rcctl enable vmd
# rcctl start vmd
```


Pour ajouter une machine virtuelle, nous procéderons toujours dans l'ordre suivant : 

+ Installation d'un système dans une machine virtuelle ;
+ Configuration de **vmd** pour qu'il gère cette machine.


==Virtualisation d'OpenBSD==


===Installation d'une machine virtuelle===

Nous allons créer en exemple une machine virtuelle avec OpenBSD dessus.
Pour nous simplifier la vie, nous créons nos machines virtuelles dans le dossier ``/var/vm`` qu'il faut créer : 

```
# mkdir /var/vm
```

Tout d'abord, créez une image disque de 50G par exemple : 

```
# vmctl create /var/vm/obsdvm.qcow2 -s 50G
vmctl: qcow2 imagefile created
```

Vous pouvez voir qu'il y a un nouveau fichier ``obsdvm.qcow2`` : c'est le disque de la machine virtuelle.


Maintenant, [installez #install] OpenBSD sur cete image. Deux possibilités : 

**Si vous avez téléchargé une image d'installation de tyle "installXX.iso"**: 

```
# vmctl start openbsdvm -m 1G -L -i 1 -r installXX.iso -d /var/vm/obsdvm.qcow2
```


Voici la signification des diverses options :

- ``vmctl start openbsdvm`` : on démarre la machine virtuelle en lui donnant le nom "openbsdvm" pour se repérer ensuite.
- ``-m 1G`` : On attribue à la machine virtuelle 1G de mémoire vive ;
- ``-L`` : Donner un accès réseau à la machine virtuelle par le biais de l'hôte ;
- ``-i 1`` : On configure une interface réseau ;
- ``-r installXX.iso`` : Chemin vers l'image iso d'installation téléchargée ;
- ``-d obsdvm.qcow2`` : Chemin vers le disque de la machine virtuelle.



**Si vous avez juste le fichier /bsd.rd** :


```
# vmctl start openbsdvm -m 1G -L -i 1 -b /bsd.rd -d /var/vm/obsdvm.qcow2
```


**Attention**, ce type d'installation nécessite un
[accès à internet #vmdnat] pour votre machine virtuelle. Configurez-le avant ;).


Une fois votre machine virtuelle démarrée, récupérer la main sur sa console ainsi: 

```
# vmctl console openbsdvm
```


L'installation d'OpenBSD se passe [comme d'habitude #install].

Une fois l'installation terminée, choisissez "Halt" puis quittez la console de la machine
virtuelle en appuyant successivement sur "``~``" et "``.``". **Attention** si
vous utiliser une connexion SSH, il faudra alors entrer la séquence "``~~.``" au
risque de vous déconnecter de votre session SSH en même temps que la console de
la machine virtuelle.


Arrêtez proprement la machine virtuelle ainsi : 

```
# vmctl stop openbsdvm
```


Maintenant que l'installation est terminée, on va simplifier la gestion de la
machine virtuelle.


===Ajout de la machine virtuelle dans vmd===

Laissons **vmd** gérer automatiquement la machine virtuelle désormais.

Éditez le fichier ``/etc/vm.conf`` qui sera lu par ``vmd`` afin de préciser quelques informations concernant la machine virtuelle : 

```
vm "openbsdvm" {
    memory 1G
    enable
    disk /var/vm/obsdvm.qcow2
    local interface
    owner batman 
}
```


Veillez à bien adapter les options "disk" et "owner" selon la configuration
souhaitée. "owner" permet de pouvoir gérer votre machine virtuelle en tant que
simple utilisateur, sans avoir à devenir root, et ça c'est cool.


Relancez **vmd** pour profiter de cette configuration. Ce dernier va démarrer
automatiquement la machine virtuelle. Vous pouvez toujours vous y connecter avec
la commande :

```
vmctl console obsdvm
```

En somme, **vmctl** est toujours disponible. :)


==Accès au réseau pour une machine virtuelle==[vmdnat]

Par défaut, les machines virtuelles sont "enfermées" sur le serveur. Si vous souhaitez leur autoriser un accès à internet, quelques manipulations sont à réaliser.


Tout d'abord, ajoutez ceci dans la configuration du parefeu (``/etc/pf.conf``)
pour autoriser le flux sortant des machines virtuelles: 

```
match out on egress from 100.64.0.0/10 to any nat-to (egress)
# utilisation d'un resolveur DNS public
pass in quick proto { udp tcp } from 100.64.0.0/10 to any port domain \
    rdr-to 1.1.1.1 port domain
```


Ici, on utilise un résolveur DNS public pour les machines virtuelles. Vous
voudrez sans doute modifier cette partie, notamment "1.1.1.1" par un résolveur
qui vous convient davantage. Lisez la [section suivante #vmddns] pour en savoir
plus.

Si vous avez configuré votre parefeu de façon à ce qu'il bloque tout par défaut,
vous devrez ajouter ceci afin d'autoriser l'interface servant à communiquer avec
les machines virtuelles : 

```
pass on tap0 from 127.0.0.1 to any
pass on tap0 from 100.64.0.0/10 to any
```


Si vous souhaitez que les machines virtuelles disposent de la même adresse IP
publique que celle de l'hôte (votre serveur), il faut la faire suivre en 
indiquant dans le fichier ``/etc/sysctl.conf``


```
net.inet.ip.forwarding=1
net.inet6.ip6.forwarding=1
```


Ainsi, les requêtes sont correctement redirigées vers (ou depuis) les machines virtuelles.

Après avoir pris en compte ces modifications, c'est tout bon :).



==Résolveur DNS pour machine virtuelle==[vmddns]

Dans la section précédente, on envoyait toutes les requêtes DNS des machines
virtuelles vers un résolveur public. Ça fonctionnera bien, mais vous préférerez
sans doute une solution "à la maison" avec unbound.

Bien sûr, vous pouvez installer et configurer unbound pour chaque machine
virtuelle. Certains peuvent trouver ça contraignant (et poser quelques soucis
lors d'une installation réseau qui nécessite de récupérer des fichiers distants).

On suppose par la suite que vous avez configuré [unbound #unbound] sur votre
serveur pour qu'il résolve les noms de domaines localement.

Afin qu'unbound accepte de résoudre les noms de domaines pour les machines
virtuelles, ajoutez cette petite ligne dans le fichier
``/var/unbound/etc/unbound.conf`` : 

```
access-control: 100.64.0.0/10 allow
```

De cette façon, unbound voudra bien communiquer avec la plage d'IP attribuées
aux machines virtuelles.

Désormais, modifier le fichier ``/etc/pf.conf`` pour indiquer de rediriger les
requêtes des machines virtuelles vers unbound : 

```
pass in quick proto { udp tcp } from 100.64.0.0/10 to any port domain \
    rdr-to 127.0.0.1 port domain
```



==Dédier une machine virtuelle à une tâche==

Vous voudrez certainement attribuer à chacune de vos machines virtuelles un rôle
bien précis : l'une pour servir les sites web, une autre pour les mails... C'est
bien entendu tout à fait possible, mais nécessite une configuration réseau
différente de celle présentée jusqu'ici.

===Ajout d'une interface virtuelle côté hôte===

Sur votre serveur, créez une nouvelle interface de type "vether" (ethernet
virtuel...)


```
# echo 'inet 10.0.0.1 255.255.255.0' > /etc/hostname.vether0
# sh /etc/netstart vether0
```


Ensuite, créez une interface de type bridge (qui fait le "pont" entre l'hôte
et les machines virtuelles).


```
# echo 'add vether0' > /etc/hostname.bridge0
# sh /etc/netstart bridge0
```


Désormais, la redirection dans le parefeu se fait ainsi : 

```
# match out on egress from 100.64.0.0/10 to any nat-to (egress) # ancienne ligne
match out on egress from vether0:network to any nat-to (egress)

# necessaire pour resolutions DNS
pass in quick proto { udp tcp } from vether0:network to any port domain \
    rdr-to 1.1.1.1 port domain

#pass on tap0 from 127.0.0.1 to any
#pass on tap0 from 100.64.0.0/10 to any
pass on vether0 from 127.0.0.1 to any
pass on vether0 from 10.0.0.0/24 to any
# ou pour ne pas s'embeter :
# pass on vether0
```


Afin de lancer une machine virtuelle, nous n'utiliserons plus l'option ``-L``
avec ``vmctl`` ni
l'option ``local interface`` dans ``/etc/vm.conf``. À la place, on configure une
adresse bien précise pour chaque machine virtuelle dans le fichier
``/etc/vm.conf`` : 


```
switch "my_switch" {
    interface bridge0
}

vm "openbsdvm" {
    memory 1G
    enable
    disk /var/vm/obsdvm.qcow2
    owner batman 
    interface { switch "my_switch" }
}
```


===Configuration du réseau côté machine virtuelle===

Sur la machine virtuelle, vous pouvez configurer une IP dans
la plage ``10.0.0.0/24`` : c'est ce qu'on a mis
dans le fichier ``/etc/hostname.vether0``. Par exemple : "``10.0.0.2``",
"``10.0.0.3``"...

La route par défaut à indiquer est ``10.0.0.1``. Cela donnera :


```
# cat /etc/hostname.vio0
inet 10.0.0.2 255.255.255.0 10.0.0.255
```


```
# cat /etc/mygate
10.0.0.1
```

```
# cat /etc/resolv.conf
nameserver 10.0.0.1
```



===Redirection du trafic vers les machines virtuelles===


Selon la nature de la demande, vous enverrez le flux sur la machine virtuelle
concernée. Prenons l'exemple suivant : 

- La machine virtuelle dont l'adresse est ``10.0.0.2`` sert un site web (ports 80
  et 443).
- La machine virtuelle ``10.0.0.3`` doit être accessible en SSH (port 22).


Sur le serveur hôte, vous configurerez le parefeu de cette façon : 


```
pass in on egress proto tcp from any to (egress) port { 80 443 } rdr-to 10.0.0.2
pass in on egress proto tcp from any to (egress) port 22 rdr-to 10.0.0.3
```


Remarquez que pour plusieurs sites web, vous voudrez plutôt utiliser **relayd**
afin de répartir la charge.



==Virtualisation de debian==


La distribution GNU/Linux debian, malgré ses nombreuses qualités, ne peut
malheureusement pas être installé via console série. Nous devons alors utiliser le
port de **qemu** uniquement pour l'installation afin de simuler la présence d'un
écran.

On installe donc qemu : 

```
# pkg_add qemu
```


Ensuite, on télécharge debian puis on l'installe après avoir créé le disque : 


```
$ ftp "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.6.0-amd64-netinst.iso"
# vmctl create /var/vm/debian.qcow2 -s 50G
# qemu-system-x86_64 -hda /var/vm/debian.qcow2 -cdrom debian*.iso -boot d
```

Malheureusement, il ne semble pas exister de solution "pratique" pour réaliser
l'installation de debian sans interface "graphique" (il y a bien quelques
bidouilles...). Autant faire l'installation sur un ordinateur puis [copier #scp]
l'image "qcow2" sur votre serveur.

Il est impératif de modifier les options de démarrage de debian une fois
l'installation terminée. Redémarrez sur le système puis éditer le fichier de
configuration de grub (le gestionnaire de démarrage). Il s'agit du fichier
``/etc/default/grub`` où vous veillerez à avoir ces lignes : 

```
GRUB_TIMEOUT=1
GRUB_CMDLINE_LINUX_DEFAULT=""
GRUB_CMDLINE_LINUX="console=tty1 console=ttyS0,115200"
GRUB_TERMINAL="console serial"
GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1"
```


Ensuite, reconstruisez la configuration de grub : 

```
# update-grub
```

Vous pouvez maintenant arrêter qemu avec la commande ``poweroff``.

Transférez l'image ``debian.qcow2`` sur votre serveur. Puisqu'elle est
volumineuse, n'hésitez pas à la "gzipper" auparavant : 


```
gzip debian.qcow2
scp debian.qcow2.gz batman@&NDD:~/
ssh batman&NDD
gunzip debian.qcow2.gz
mv debian.qcow2 /var/vm/
```




On peut maintenant configurer la machine virtuelle sur le serveur dans ``/etc/vm.conf``.

```
switch "my_switch" {
    interface bridge0
}

vm "debianvm" {
    memory 200M
    enable
    disk /var/vm/debian.qcow2
    interface { switch "my_switch" }
    owner batman
}
```

Et voilà, vous avez debian virtualisée par OpenBSD :).


==Aller plus loin==

- [FAQ officielle https://www.openbsd.org/faq/faq16.html]
- [openbsd.amsterdam https://openbsd.amsterdam/] propose des machines virtuelles
  et reverse une partie des gains à OpenBSD (1/6e tout de même ! :)). Toute leur infrastructure est 
  [détaillée https://openbsd.amsterdam/setup.html]. Si vous le voulez, vous
  pouvez indiquer mon code référent (**referral code**) : ``OBSD-2609-VEXD``.



Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)



=Introduction=


==Avant-propos==

Vous êtes sur le point de plonger dans l'univers de l'auto-hébergement.
Ce document est écrit pour vous aider à héberger chez vous ou sur un serveur
dédié certains services
malheureusement trop souvent confiés à des tiers. 
L'objectif est de vous guider dans la découverte de ces notions de
façon cohérente, sans fioritures et aussi simplement que possible.
Vous comprendrez donc que vulgariser
l'auto-hébergement entraîne des compromis. Les plus pointilleux chercheront certainement à
approfondir les notions évoquées ici. C'est pourquoi vous pouvez lire les
parties
dans l'ordre que vous souhaitez, selon vos besoins, quitte à faire quelques rappels parfois.
Le sommaire est là pour vous repérer. ;)

Afin que ce voyage soit le plus confortable possible, nous utiliserons le 
système [OpenBSD https://www.openbsd.org]. Ce dernier est réputé pour être sûr. 
Il est aussi, à mon avis, nettement plus simple à configurer que
d'autres systèmes. En effet, les serveurs mail, http, antispam (entre autres) sont déjà intégrés
et se configurent avec une syntaxe semblable. Souhaitant rendre
plus accessible la démarche d'auto-hébergement, c'est un avantage non
négligeable.

Bien que les développeurs d'OpenBSD fournissent de nombreux efforts
pour vérifier le code source et assurer un système fiable et sécurisé, il faut
rester vigilant. Pas de panique, nous verrons quelques
éléments de rigueur pour configurer en toute sécurité votre serveur, cela demande juste du bon
sens.

Vous verrez que s'auto-héberger n'est finalement pas si difficile et
consiste en grande partie à modifier du texte dans des fichiers. Cette démarche
devrait donc être accessible à tous. 

Alors, prêt à plonger?
Bonne lecture!



==L'auto-hébergement : C'est quoi? Avantages et inconvénients.==


La plupart des sites web que vous avez l'habitude de consulter (vos courriels,
les réseaux sociaux...) sont hébergés sur des ordinateurs quelque part dans le
monde. Ces derniers ne **servent** qu'à proposer des services et du contenu à
d'autres ordinateurs. On les appelle des **serveurs**. La seule différence
notable est qu'ils ne sont en général pas reliés à un écran.

Lorsque vous voulez consulter vos e-mails, votre navigateur va chercher sur un
serveur tous vos messages, qui sont alors téléchargés
vers votre ordinateur.  C’est comme si pour lire votre courrier postal, vous
deviez aller dans une agence pour demander au facteur :

	Y a-t-il du courrier pour moi?

Ce dernier va vérifier, puis prendre votre courrier pour vous le donner.
C’est le bureau de poste qui l'avait avant que vous ne
le releviez. Le principe n'est pas si différent lorsque vous allez consultez vos
mails.

Ce procédé apporte quelques inconvénients. Imaginez que le facteur ouvre votre
courrier avant de vous le donner... 

La centralisation des services
permet aux sociétés qui façonnent le web 
de contrôler les données à leur guise, pas toujours dans l'intérêt des utilisateurs. Qui n'a pas râlé parce
qu'il n'obtenait pas ce qu'il espérait d'un service postal? C'est la même chose
pour Google (au hasard) qui met en avant certains contenus, par exemple leurs
services commerciaux ; scanne vos mails pour vous proposer des publicités
susceptibles de vous plaire... À ce jour, ce type d'activités n'est plus un secret.

De plus, si l'un de ces fournisseurs tombe en panne, c'est tout une partie de
l'Internet qui est inaccessible aux dépens des utilisateurs. Cela va à
l'encontre de l'idée originale du web où chacun devrait pouvoir constituer un
nouveau noeud à sa toile.


Héberger chez soi les services que l'on utilise présente
plusieurs avantages :

- Les données restent chez vous. Cela veut dire que vous en gardez le contrôle. 
  C'est particulièrement intéressant si vous aviez l'habitude de
  partager des documents à l'aide de services tiers (les photos chez Picasa,
  les vidéos sur Youtube, des papiers administratifs sur WeTransfer ou Dropbox...). Vous êtes ainsi certains que
  vos données n'ont pas été oubliées sur un disque dur dans une quelconque 
  déchetterie après un renouvellement du matériel, voire revendues au plus offrant.
- Votre vie privée est davantage respectée. Par exemple, vous êtes sûrs que vos courriels ne seront
  pas scannés. Cela est primordial, même lorsque l'on pense 
  [ne rien avoir à cacher https://jenairienacacher.fr/].
	Prétendre que votre droit à une sphère privée n'est pas important parce que vous n'avez rien à cacher n'est rien d'autre que dire que la liberté d'expression n'est pas essentielle car vous n'avez rien à dire.
	-- E. Snowden

- Vous pouvez avoir à portée de main des services qui répondent exactement à vos
  besoins.
- Vous pouvez utiliser du matériel à faible consommation électrique et faire
  ainsi attention à la planète.
- S’auto-héberger, c’est amusant, instructif et passionnant. Cela permet de mieux comprendre
  le fonctionnement d’Internet et de participer à sa qualité.


Cependant, cette démarche n'est pas sans inconvénients : 

- Cela prend du temps.
- La bande passante des particuliers n'est pas toujours aussi grande que ce que
  l'on voudrait. Vous ne
  pourrez donc pas envoyer des données aussi vite que vous le souhaitez.
  Ça peut être gênant pour transférer des fichiers //très// volumineux.
  (vidéos...). Pour une utilisation familiale, nul besoin de la fibre toutefois.
- C'est à vous de vous charger de la sécurité. Cela demande du soin,
  mais vous avez au moins la certitude que ce n'est pas effectué avec
  négligence ou par un inconnu.


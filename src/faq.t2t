Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

==FAQ : Foire aux questions==[faq]

===Comment lire et chercher dans du texte ?===

Pour seulement consulter un fichier, utilisez la commande ``less``.

Ensuite, vous pouvez chercher n'importe quelle chaîne de caractères en appuyant
sur ``/`` puis en écrivant votre recherche. Appuyez sur ``n`` pour aller à
l'occurrence suivante, ou bien ``N`` pour revenir en arrière.

Pour quitter ``less``, appuyez sur ``q``.

Si vous voulez chercher dans le contenu des [logs #logs], ça peut être pratique ;).


===Comment modifier un fichier?===[editfile]

Il existe une ribambelle d’éditeurs de texte (vim, nano...). L'éditeur
par défaut sur OpenBSD est **[vi https://man.openbsd.org/OpenBSD-current/man1/vi.1]**.
Il est peut être étonnant à utiliser au
premier abord, si bien que certains voudront peut-être installer un autre
éditeur à la place.
Cependant, vi est très pratique une fois qu'on l'a pris un peu en
main. Si au contraire vous êtes déjà habitué à l'éditeur emacs, vous trouverez
votre bonheur avec l'éditeur **mg** présent lui aussi par défaut (voir le 
[manuel https://man.openbsd.org/OpenBSD-current/man1/mg.1] pour plus de
détails).

Voici quelques conseils pour utiliser vi au travers d'un exemple.
Pour éditer le fichier ``/etc/iloverocknroll``, vous
saisirez ceci :

```
$ vi /etc/iloverocknroll
```

Apparaîtra alors le contenu de ce fichier dans le terminal :

[img/iloverocknroll.png]


En général, vous procéderez seulement ainsi : 

+ Appui sur la touche "i" pour pouvoir écrire.
+ Appui sur "echap" pour quitter le mode insertion.
+ Enregistrement et fermeture de vi en saisissant ``:wq``.



Vous êtes toujours là ? :)

Allons donc un peu plus loin (mais pas trop, promis ^^).
Comprenez tout de suite qu'il existe trois modes : 
- Le mode "visualisation" : vous pouvez vous déplacer dans le fichier avec les
  touches "h","j","k","l".
- Le mode "insertion" : vous pouvez écrire du texte. On entre dans ce mode avec la
  touche "i". Pour en sortir, on appuie sur "echap".
- Le mode "édition" : ce dernier est moins utile lorsqu'on débute. On peut faire
  des modifications rapides, par exemple remplacer du texte ou supprimer
  plusieurs lignes d'un coup.


Pour enregistrer les modifications, appuyez sur ``:`` puis sur ``w``.
Validez avec entrée.
On peut maintenant quitter en écrivant ``:q``. Notez que vous pouvez aller
plus vite en saisissant directement ``:wq``.

Afin de chercher un texte, ce qui est bien utile dans les gros fichiers,
appuyez sur la touche ``/`` puis écrivez votre recherche.

Si vous souhaitez quitter sans enregistrer vos modifications, saisissez
alors ``:q!``.

Deux autres astuces bien pratiques : 

- ``cw`` : permet de changer un mot (change word)
- ``c$`` : permet de changer du curseur jusqu'à la fin de la ligne
- ``3G`` : permet d'aller à la ligne numéro 3
- ``ma`` : place une marque 'a' à l'emplacement du curseur. Pour y revenir
  rapidement ensuite, vous entrerez ``'a``.
- ``dd`` : supprime la ligne.
- ``yy`` : copie la ligne.
- ``p`` : colle ce qui a été copié ou supprimé avant.
- ``x`` : supprime un caractère sous le curseur
- ``u`` : (undo) annule l'action précédente.
- ``.`` : répète l'action précédente. Pour annuler plusieurs fois, vous ferez
  donc ``u....`` (ici, 5 fois en tout).



Dernière astuce pour coller du texte. Entrez en mode "édition" en appuyant sur
``i``. Sélectionner avec la souris le texte à coller. Dans vi, appuyez sur 
``Shift-Insert`` : le texte est collé.


**Inutile de tout retenir**, ça rentrera petit à petit.

Cela devrait être amplement suffisant dans un premier temps. Si le fonctionnement
de vi vous intéresse, vous pouvez consulter 
[cette documentation https://man.openbsd.org/vi#FAST_STARTUP].



===Quel matériel acheter ?===

Vous n'avez pas besoin d'une puissance phénoménale pour vous auto-héberger.
Souvent, une machine récupérée car tro peu puissante pour une utilisation
bureautique sera amplement suffisante.

Si vous souhaitez investir, je vous conseille de vérifier la compatibilité avec
OpenBSD. Ça évite une mauvaise surprise et un coup de pas-de-chance :).

La liste du matériel supporté, classé par architecture, est disponible sur
la [FAQ officielle (en) https://www.openbsd.org/faq/faq1.html#Platforms]
et [la FAQ inofficielle (fr) https://wiki.obsd4a.net/openbsd.org:plat].

Mon petit coup de coeur ? Les architectures arm car très peu gourmandes en
énergie.
Bien que ça ne soit pas une architecture ARM, la carte 
[APU2 https://pcengines.ch/apu2.htm] (apu2d0) est économe, peu chère, fiable et
fonctionne vraiment très bien ! Vous trouverez la liste des vendeurs 
[ici https://pcengines.ch/order.htm].




===Un vieil ordinateur, ça suffit ?===
    
	Je dispose d'un vieil ordinateur. Je pense que l’alimentation n’est 
	pas adaptée pour le laisser allumé 24 h/24, 7 j/7, non ?

Pour la consommation, regardez ce qui est écrit sur l'alimentation. La
puissance en watt donnera une idée de la consommation engendrée. En
général, une puissance de 1W, c'est environ 1 euro/an d'électricité.

===Quid d'OpenBSD sur Raspberry Pi ?===

	Est-ce qu'OpenBSD peut fonctionner sur le raspberry pi ?

Oui, à partir de la version 3 du raspberry pi en choisissant l'architecture
[arm64  https://www.openbsd.org/arm64.html]. Cela reste très jeune et difficile
[à utiliser http://undeadly.org/cgi?action=article&sid=20170409123528]
pour l'instant malgré le travail en cours. Vous pourrez
préférer une autre carte ARM dans ce cas.

===Le modem de mon fournisseur suffit ?===
	Est-ce qu’une « bête » *Box (Freebox) suffit pour assurer convenablement un
	tel service sur la durée ?

Oui !
Si vous préférez, vous pouvez bien entendu utiliser votre propre routeur.


===C'est quoi un sous-domaine?===

**Réponse rapide** : un domaine c'est ``&NDD``. Un sous-domaine
c'est ``sousdomaine.&NDD``.

**Réponse longue** : Le domaine principal est enregistré chez votre registre
par un champ A pointant vers l'adresse IP de votre serveur. Ensuite, vous
pouvez ajouter autant de sous-domaines que vous voulez en créant dans la zone
des champs de type CNAME, qui renvoient vers le champ A précédent. En gros,
les visiteurs sont toujours dirigés vers votre serveur, mais selon le
domaine/sous-domaine entré dans la barre d'adresse, votre serveur leur fourni un
contenu différent.

Un champ CNAME se présente donc sous cette forme : 
```
sousdomaine    IN CNAME  domaine.net.
```

Le point final est important !

Tout est bien expliqué [dans cette partie #principes-DNS].

===Quelle taille pour le disque dur?===

	Il paraît que pour un serveur, on peut utiliser un vieux PC. Mais quelle
	taille de disque dur est nécessaire au minimum?

Ça dépend. La réponse ne sera pas la même selon ce que vous voulez
faire du serveur. 
Pour un petit site web, quelques Go suffiront
amplement. 

Si c'est un site contenant images et vidéos à gogo,
alors il faudra un espace plus conséquent. 

Si votre serveur fait office de
seedbox ou de mediacenter, alors il faut compter encore plus.

Dans la majorité des cas, 10G à 20G seront suffisants.

===Quelle puissance pour le processeur?===

Là aussi, ça dépend.
Pour un simple site avec quelques visites par jour, peu de
puissance est nécessaire.
    
S'il y a du PHP, il faut alors une puissance un peu plus élevée.

Enfin, si le serveur propose de la messagerie instantanée, un site
avec PHP, des mails... Vous l'aurez compris, il faudra encore
une puissance plus grande. 

Il en va de même pour la mémoire vive d'ailleurs.

Dans la majeure partie des cas, un vieux pc récupéré à la configuration modeste
sera suffisant, vous investirez si des ralentissements apparaissent.


===Je n'ai pas d'IP fixe===
	Mon fournisseur d'accès ne me donne pas une adresse IP fixe.

C'est effectivement embêtant. Car dans ce cas, l'adresse IP associée à votre
domaine "&NDD" doit régulièrement être mise à jour.

Sachez qu'il existe pour ça DynDNS. C'est un service qui
permet de réaliser cette opération, et la plupart des *box
ont une option pour le configurer.

Cela peut toutefois être fastidieux, et vous préférerez
peut-être changer de fournisseur d'accès.
[FDN http://www.fdn.fr] par exemple?


===Où trouver des applications pour mon serveur?===

Le site [WAAH http://waah.quent1.fr/], Wiki des
Applications Auto-Hébergées recense de nombreuses applications
que l'on peut héberger sur son serveur. Il en existe sûrement
d'autres, mais celui-ci est particulièrement complet.

Il existe aussi 
[alternative.to http://alternativeto.net/platform/self-hosted/]
qui recense quelques projets.

En anglais, on peut évoquer 
[ce dépôt github https://github.com/Kickball/awesome-selfhosted]
qui recense de nombreuses
applications à auto-héberger.


===Où vont mes mails si mon serveur est éteint ?===

	Quand on héberge ses mails, que se passe-t-il quand le serveur de mail est, pour une raison quelconque, inaccessible ?

Voilà une question qu'on se pose forcément à un moment donné. Lorsque
le serveur est inaccessible, il se passe à peu près la même chose que
lorsque le facteur doit délivrer un recommandé : le serveur
expéditeur essaie une première fois et échoue. Il va repasser une fois
suivante un peu plus tard. En cas de second échec, il réessaie à nouveau
plus tard.

Bien évidemment, après plusieurs échecs, il abandonne.

Vous devez vous demander combien d'essais et quels intervalles entre chaque
essai? À cela je n'ai pas de réponse précise, car ça dépend de la
configuration du serveur expéditeur. On peut toutefois raisonnablement
considérer que les essais peuvent se poursuivre pendant une dizaine de
jours.


===Que faire contre les coupures de courant ?===

Plusieurs astuces sont possibles : 

- Solution facile : utiliser un onduleur. C'est une sorte de batterie de
  secours en cas de coupure de courant. On y branche dessus le routeur
  qui donne accès à Internet et le serveur. Mais bon, puisque les mails
  sont ré-envoyés en cas de pépin, ce n'est peut-être pas utile.
- Avoir un copain qui héberge un enregistrement MX de secours
  correspondant à celui de son serveur. Il "stockera" les mails en
  attendant que votre serveur soit accessible. À nouveau, ça reste inutile
  pour des courtes coupures de courant.


Dans le cas où l'interruption du serveur est due à un bug
quelconque, sachez que tous les jours, OpenBSD génère un rapport de
sécurité qui peut dire : "ce service devrait être en route mais ne
l'est pas". Ainsi, vous pouvez aller vérifier assez rapidement quel est le
problème.


En somme, pas de quoi s'alarmer outre mesure.


===Comment connaître l'utilisation et les vitesses réseau ?===

Vous pouvez installer les ports ``iftop`` ou ``bwm-ng``.

Sinon, présent de base sur OpenBSD, il y a la commande ``systat`` : 

```
# systat states
```

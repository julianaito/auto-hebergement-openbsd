Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8


==TOR==

[Tor https://www.torproject.org/] est un logiciel libre permettant de renforcer
la vie privée de ses utilisateurs et ainsi passer outre les surveillances
subies lors de l'utilisation d'Internet. Lorsqu'on l'utilise, les communications
sont réparties à travers une maille de serveurs, afin d'obtenir un //onion router//.
En gros, ce que vous demandez sur le web circule entre une série de serveurs (les
couches de l'oignon), ce qui rend très difficile de savoir d'où viennent les
paquets, et donc de vous localiser! 


===Configurer un relais===


Il vous est possible de participer à ce réseau en étant un
serveur relais. Qui plus est, cela rendra d'autant plus difficile de déterminer vos
propres activités, puisque vos centres d'intérêt seront noyés
parmi le trafic sortant de votre connexion.

Tor peut avoir besoin d'ouvrir de nombreuses connexions. Réduire les limitations
peut alors être une bonne idée. Ajoutez dans le fichier ``/etc/sysctl.conf``

```
kern.maxfiles=20000
```

Installez et activez Tor ainsi : 
```
# pkg_add tor
# rcctl enable tor
```

Assurez-vous d'ouvrir dans votre pare-feu, et de rediriger dans
votre routeur le port 9001.


Ensuite, éditez le fichier ``/etc/tor/torrc`` , afin d'obtenir ces quelques lignes :

```
SOCKSPort 0
ORPort 9001
Nickname Surnom
RelayBandwidthRate 75 KB  
RelayBandwidthBurst 100 KB 
ContactInfo votrenom <adresse AT email dot fr>
ExitPolicy reject *:* # no exits allowed
```

Modifiez les valeurs pour ``RelayBandwidthRate`` et
``RelayBandwidthBurst`` selon votre accès à Internet. Il s'agit de la bande
passante que vous laissez disponible pour Tor. C'est une bonne idée de préciser
votre adresse mail aussi.

Enfin, démarrez Tor avec ``rcctl start tor`` et attendez de voir apparaître dans
le fichier ``/var/log/messages`` : 

```
May 12 12:20:41 chezmoi Tor[12059]: Bootstrapped 80%: Connecting to the Tor network
May 12 12:20:41 chezmoi Tor[12059]: Bootstrapped 85%: Finishing handshake with first hop
May 12 12:20:42 chezmoi Tor[12059]: Bootstrapped 90%: Establishing a Tor circuit
May 12 12:20:44 chezmoi Tor[12059]: Tor has successfully opened a circuit. Looks like client functionality is working.
May 12 12:20:44 chezmoi Tor[12059]: Bootstrapped 100%: Done
May 12 12:20:44 chezmoi Tor[12059]: Now checking whether ORPort 109.190.xxx.xxx:9001 is reachable... (this may
take up to 20 minutes -- look for log messages indicating success)
May 12 12:21:10 chezmoi Tor[12059]: Self-testing indicates your ORPort is reachable from the outside. Excellent. Publishing server descriptor.
May 12 12:21:12 chezmoi Tor[12059]: Performing bandwidth self-test...done.
```


===Configurer un service caché===

Vous pouvez proposer votre site web (ou n'importe quel autre service) au travers
du réseau Tor. Ceux qui voudront y accéder utiliseront une adresse se terminant
par ".onion", comme "5rud2tr7sm3oskw5.onion".

Avant d'aller plus loin, notez qu'il est **très fortement déconseillé** d'héberger
un relais et un service caché en même temps.

Ceci étant dit, vous pouvez activer votre site caché en éditant le fichier
``/etc/tor/torrc``. Décommentez les lignes correspondantes ou ajoutez-les : 

```
SOCKSPort 0
HiddenServiceDir /var/tor/hidden/
HiddenServicePort 80 127.0.0.1:80
```

Relancez Tor pour activer ce service caché : ``rcctl restart tor``.
Deux nouveaux fichiers vont apparaître dans le dossier ``/var/tor/hidden/`` :
"hostname" et "private_key". L'adresse de votre site en .onion se trouve dans le
fichier hostname. Notez-la : 

```
# cat /var/tor/hidden/hostname
5rud2tr7sm3oskw5.onion
```

Cependant, ne communiquez jamais le contenu de ``private_key``.

Il ne nous reste plus qu'à configurer [httpd #httpd] pour lui dire de recevoir les
connexions vers l'adresse en ".onion" et de les servir. Le fichier
``/etc/httpd.conf`` pourra alors contenir ceci : 

```
server "5rud2tr7sm3oskw5.onion" {
        listen on 127.0.0.1 port 80
        # emplacement du site
        root "/htdocs/&NDD"     
        directory index index.html

        [...]
}
```

Vous pouvez tester votre site (après un ``rcctl reload httpd`` bien sûr) avec le 
[navigateur torbrowser https://www.torproject.org/projects/torbrowser.html.en].


	Mais, ce n'est pas chiffré dans une adresse https! Est-ce vraiment sécurisé?

Bonne remarque. Le chiffrement TLS n'est pas nécessaire ici, puisque
le tunnel ouvert par Tor pour accéder au site est entièrement chiffré. De plus,
le navigateur devrait valider le certificat, or, ce dernier n'est pas enregistré
pour un domaine en ".onion". Notez que si vous pouvez obtenir un certificat pour
cette adresse, c'est alors possible de configurer un accès en https.

Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8


Devenir responsable de ses communications est un pas de géant vers l'autonomie
et la liberté.
Cette partie explique l'installation d'un serveur de courriel à la maison.
Aucune base de données ne sera utilisée, puisque cela ne représente aucun
intérêt en auto-hébergement. Restons simples !

La mise en place d'un serveur mail est souvent considérée comme délicate. On va
donc détailler l'installation pas à pas en petites étapes simples :

- Configuration d'enregistrements DNS particuliers ;
- Création des certificats pour sécuriser l'identification avec le
  serveur ;
- Préparation pour les utilisateurs virtuels, rien de compliqué :) ;
- Configuration d'opensmtpd, qui se charge d'envoyer et recevoir votre courrier ;
- Configuration de dovecot, qui permet la réception du courrier avec un client comme 
[Thunderbird https://www.mozilla.org/fr/thunderbird/] ;
- Faire le nécessaire pour que vos messages ne soient pas considérés comme des
  spams ;
- Installer un antispam.


Nous verrons en passant comment ajouter de nouveaux comptes mail sur votre
serveur et comment configurer votre client de messagerie pour l'utiliser.

%FIXME : à vérifier
Par défaut, si vous ne touchez à rien, les démons de votre serveur sont capables d'envoyer des
messages vers l'extérieur. Ça reste limité et n'a rien à voir avec un service de
messagerie complet.

Notez que certains fournisseurs d'accès à internet bloquent les mails sortant
(port 25). Renseignez-vous avant d'avoir une mauvaise surprise. À défaut,
regardez une proposition 
[au paragraphe parlant de service smtp externe #remotesmtp].


Je vous propose deux configurations : 

- [Pour les pressés #mailpresse]; une configuration en 10 minutes.
- Une configuration plus souple avec [**utilisateurs virtuels** #mailvirtuals].
C'est à la fois plus simple à entretenir et plus sûr.


==Configuration de votre zone DNS pour les mails==

Le système de mail passe par des enregistrements qui lui est propre. Nous devons
donc procéder à quelques modifications chez notre
[registre #registre] ou dans notre [zone #principes-DNS]. Ajoutez deux nouveaux champs : 

- Un champ de type A qui pointe vers votre [IP #IP] (sans doute déjà présent): 
```
&NDD IN A &IPV4
```
Bien sûr, remplacez ``&IPV4`` par votre IP.
- Un champ de type MX qui pointe vers le A précédent 
```
&NDD. IN MX 1 &NDD.
```

Notez l'importance du "``.``" final.


Certains spécifient un champ A différent du domaine principal afin de gérer des
backups (serveurs de mail secondaires) plus tard. Ce n'est **pas nécessaire du tout**, mais si vous y tenez, voici à quoi ça
ressemblera : 


```
mail1.&NDD IN A &IPV4
mail1.&NDD. IN MX 1 mail1.&NDD.
```


Ça sera tout pour cette partie.


==Création des certificats==
Ces certificats permettront de chiffrer la communication entre votre serveur et
le logiciel qui récupère vos
mails. De plus, cela rend chaque communication authentique.

Pour obtenir ou créer des certificats, je vous renvoie à la 
[partie consacrée #acmeclient]. Que vous utilisiez ``letsencrypt`` ou bien des
certificats auto-signés n'a pas grande importance, les deux fonctionnent très
bien. Veillez juste à bien prendre note de l'emplacement
des certificats sur votre serveur.

Par la suite, nous feront comme si nous utilisions les certificats obtenus avec
letsencrypt.

==Un serveur mail en 10 minutes==[mailpresse]

Voici une configuration pour les plus pressés. Avec cette dernière : 
- Les comptes mail sont les utilisateurs du système. Un compte utilisateur = un compte mail.
- Les mails seront enregistrés dans un dossier ``~/Maildir`` situé dans le dossier personnel de chaque utilisateur.
- Un utilisateur "toto" aura pour adresse mail "toto@&NDD".
- Pour ajouter un compte, référez-vous à la commande [adduser #adduser] Le shell ``/sbin/nologin`` sera de préférence à attribuer dans la plupart des cas.
- Il ne vous est pas possible de gérer plusieurs domaines.
- C'est plus difficile à gérer si vous avez de nombreux utilisateurs. Pour une dizaine seulement, c'est amplement suffisant et efficace.


La configuration sera plus détaillée et expliquée 
dans la partie avec [utilisateurs virtuels #mailvirtuals]. Ici, c'est pour les pressés.

C'est parti ? :)


===Fichier smtpd.conf===
Voici le contenu du fichier ``/etc/mail/smtpd.conf`` : 


```
# Configuration generale
## Tables 
table aliases file:/etc/mail/aliases

## Certificats
pki &NDD key "&SSLKEY"
pki &NDD cert "&SSLCERT"

### Messages locaux
listen on lo0 

### Reception
listen on egress tls pki &NDD 
### Envoi avec client de messagerie
listen on egress port submission tls-require pki &NDD auth 


# ACTIONS
action "envoi" relay 

## On applique les alias systeme
action local_maildir maildir alias <aliases>
## On delivre l'enveloppe dans un dossier maildir
action in_maildir maildir 

# Correspondances
## Reception
### Message du systeme pour les utilisateurs locaux
match for local action local_maildir
### Message pour les utilisateurs virtuels
match from any for domain "&NDD" action in_maildir

## Envoi
match for any action "envoi"
match auth from any for any action "envoi"
```


C'est tout :)


===Configuration de dovecot===

Installez dovecot avec ``# pkg_add dovecot``.

Dans le fichier ``/etc/dovecot/local.conf``; indiquez : 

```
# On écoute en IPV4 et IPV6.
listen = *, [::]

# On stocke en Maildir
mail_location = maildir:~/Maildir

# imap > pop
protocols = imap

# Securisation. Editez ces lignes
ssl = yes
ssl_cert = <&SSLCERT
ssl_key = <&SSLKEY
disable_plaintext_auth = yes

# methodes d'authentification
passdb {
        driver = bsdauth
}

userdb {
        driver = passwd
}
```


Et voilà !
Vous étiez prévenus, c'est fait en 10 minutes.

Si vous voulez davantage de détails, notamment
sur [dovecot #dovecot], allez lire la suite. ;)


==Serveur mail avec utilisateurs virtuels==[mailvirtuals]
===Préambule===
Cette désignation fait référence à des utilisateurs
qui sont bien réels, mais qui ne sont pas des comptes UNIX à proprement parler
qui peuvent accéder à un shell (la ligne de commande).

Ainsi, un nouveau compte mail ne sera pas créé avec ``adduser``, mais en éditant
un simple fichier texte contenant le nom d'utilisateur et un hash de son mot de
passe.

Dans tous les cas, vérifiez bien que le paquet ``opensmtpd-extras`` est installé. Il est nécessaire pour profiter des tables de mots de passe.


====Un utilisateur responsable des mails : _vmail====

On va créer utilisateur en charge de tous les mails. Il portera le doux nom de
"``_vmail``" ^^.  Ce dernier ne servira qu'à
ça et n'aura donc pas accès au shell, c'est plus sûr :) :

```
# useradd -m -g =uid -c "Virtual Mail" -d /var/vmail -s /sbin/nologin _vmail
```


Un nouveau dossier est créé : ``/var/vmail``.
Les messages des utilisateurs seront dedans, mais bien organisés : 
dans ce dossier, il y aura des sous-répertoires portant le nom des utilisateurs
virtuels. Ainsi, les messages seront enregistrés dans, par exemple :

```
/var/vmail/&NDD/batman/Maildir
/var/vmail/&NDD/utilisateur/Maildir
/var/vmail/&NDD/ninja/Maildir
...
```

====/etc/mail/virtuals====

Ce fichier contient la liste des utilisateurs, un par ligne. Il fonctionne
comme le fichier ``/etc/mail/aliases`` :

```
heros@&NDD batman@&NDD,superman@&NDD
batman@&NDD _vmail
superman@&NDD _vmail
kiki@&NDD _vmail
```

Eh oui, toutes ces adresses vont pour l'utilisateur ``_vmail``.

Notez que sur la première ligne, on a fait un alias.


====/etc/mail/passwd====

Même logique ici, une ligne pour un mot de passe : 

```
batman@$NDD:$2b$09$lerdFpdQtnu.Bs5EpAsVbeF851GjdD0aza8IDhho38i1DOHk.ujzi::::::
superman@$NDD:$2b$09$VRU/CYJUS3QZHVUFP70xIOURPbiNQeyOEZHoZo6NOY3uO.XSpd2MW::::::
```

Chaque ligne est constituée des éléments suivants, séparés par des ``:`` : 

- L'adresse mail du compte ;
- Le mot de passe chiffré ;
- Des options. Dans cet exemple, on n'en a pas précisé, il y a donc 6 ``:`` en
  fin de ligne.


Afin de chiffrer le mot de passe, utilisez la commande ``encrypt`` ainsi : 

```
encrypt -p
```

Ça vous demande d'entrer le mot de passe. Lorsque vous validez avec "Entrée",
un hash du mot de passe s'affiche, il reste à le mettre dans
``/etc/mail/passwd``.


====Permissions des fichiers d'identification====

Les fichiers précédents ne doivent pas être lisibles par un simple utilisateur
de passage. Faisons en sorte que seul l'administrateur (root) puisse écrire
dedans et rendons-les lisible par les démons qui en auront besoin : dovecot et
smtpd. Ce n'est pas une obligation, mais c'est une précaution qui ne peut
blesser personne ;).

Afin de séparer les privilèges, dovecot et smtpd fonctionnent à partir
d'utilisateurs aux accès restreints, respectivement ``_smtpd`` et ``_dovecot``.
Tout ceci permet de protéger votre serveur si un jour, l'un de ces services
était compromis.

Nous allons créer un groupe ``_maildaemons`` dans lequel nous mettrons les deux
utilisateurs cités ci-dessus afin de faciliter la gestion des permissions : 

```
# groupadd _maildaemons
# usermod -G _maildaemons _smtpd
# usermod -G _maildaemons _dovecot
```

**dovecot** n'est peut-être pas installé à [ce stade de la lecture #dovecot],
installez-le avant la dernière commande : ``# pkg_add dovecot``.


On définit maintenant le propriétaire et le groupe des fichiers contenant les mots de
passe et identifiants : 

```
# chown root:_maildaemons /etc/mail/passwd /etc/mail/virtuals
```


Enfin, on ne permet qu'à ``root`` d'écrire dans ces fichiers et au groupe
``_maildaemons`` d'en lire le contenu :


```
# chmod 640 /etc/mail/passwd /etc/mail/virtuals
```

Si vous vérifiez, vous voyez que les [permissions #permissions] et 
[propriétaires #owner] sont corrects : 


```
# ls -l /etc/mail/passwd
-rw-r-----  1 root  _maildaemons  17226 Nov 12 08:40 /etc/mail/passwd
```



===Configuration d'Opensmtpd===
Opensmtpd (//smtpd//) est le serveur mail par défaut sur OpenBSD. Il est déjà
installé, reste à le configurer. 

Cependant, avant toutes choses, ouvrez et redirigez les ports suivants :
25 (smtp), 587 (submission) et 993 (imaps). Nous ne préoccupons pas du port 465
(smtps) car il est [déprécié https://en.wikipedia.org/wiki/SMTPS]. 


Pour configurer opensmtpd, on édite
``/etc/mail/smtpd.conf``. Ce dernier sera mis en oeuvre dans 
**l'ordre de lecture**.

Ce dernier se décompose en 3 parties : 

- Les options générales du serveur ;
- Les actions qui pourront être réalisées sur les mails, qu'on appelle
  "enveloppes" ;
- Les critères pour reconnaître les enveloppes et y appliquer les actions qui
  correspondent.


Le voici, à adapter à vos besoins : 

```
# Configuration generale
## Tables 
table aliases file:/etc/mail/aliases
table passwd passwd:/etc/mail/passwd
table virtuals file:/etc/mail/virtuals

## Certificats
pki &NDD key "&SSLKEY"
pki &NDD cert "&SSLCERT"

## Ecoute pour recevoir/envoyer
### Messages locaux
listen on lo0 
### Reception
listen on egress tls pki &NDD 
### Envoi avec client de messagerie
listen on egress port submission tls-require pki &NDD auth <passwd> 

# ACTIONS 
action "envoi" relay 
action local_maildir mbox alias <aliases>
action virtual_maildir maildir "/var/vmail/%{dest.domain}/%{dest.user}/Maildir" virtual <virtuals>

# Correspondances
## Reception
### Message pour les utilisateurs locaux
match for local action local_maildir
### Message pour les utilisateurs virtuels
match from any for domain &NDD action virtual_maildir

## Envoi
match auth from any for any action "envoi"
match for any action "envoi"
```



Vous n'avez quasiment rien à modifier dans ce fichier, mis à part ``&NDD`` à remplacer par
votre nom de domaine.

	STOOOP! On veut des détails!


Regardons les lignes de ce fichier les unes après les autres. 

Tous d'abord, les premières lignes correspondent à des options générales.

- ``table aliases ...`` : On précise dans quel fichier se trouvent les alias
  entre les utilisateurs. Ce fichier permet de [faire suivre des messages #admin].
- ``table passwd ...`` : On définit le fichier contenant les mots de passe
  chiffrés qu'on a créés auparavant.
- ``table virtuals ...`` : Le fichier contenant la liste des utilisateurs
  virtuels.
- ``pki ...`` : On indique où se trouve la clé et le certificat correspondant
  servant à identifier le serveur. Modifiez les emplacements selon ce que vous
  avez obtenu [dans le paragraphe sur la gestion des certificats #sslcert].
- ``listen on lo0`` : le serveur mail va écouter en local au cas où le système
  envoie des messages.
- ``listen on egress tls pki &NDD`` : //smtpd// écoute (sur le port 25) pour
  recevoir des courriels d'autres serveurs.
  Ici, la connexion profite du chiffrement tls si
  possible en
  utilisant le certificat configuré plus haut, repéré par son nom de domaine.
- ``listen on egress port submission tls-require pki &NDD auth <passwd>`` :
  Cette ligne indique que le serveur écoute sur le port submission avec
  indispensablement une connexion chiffrée en tls (``tls-require``). Aussitôt, celui qui se
  connecte sur ce port doit s'identifier (``auth``) par rapport au contenu de la teble
  ``<passwd>``. Cette ligne permet à un utilisateur virtuel de se servir d'un client de
  messagerie pour envoyer des courriels à partir de son ordinateur. 


Ensuite, sont définies quelques actions qui seront appliquées ensuite aux
mails. Il peut s'agir d'envoyer un courriel à l'extérieur ou bien de distribuer
une enveloppe à un utilisateur du serveur.

- ``action "envoi" relay`` : le serveur relaie l'enveloppe. Dit plus simplement,
  il l'envoit à l'adresse écrite dessus, au serveur mail SMTP du destinataire.
- ``action local_mbox mbox alias <aliases>`` : On distribue l'enveloppe dans une
  boîte de type mbox d'après la table de correspondance ``<aliases>``. C'est
  utile pour les messages internes au système.
- ``action virtual_maildir maildir "/var/vmail/%{dest.domain}/%{dest.user}/Maildir" virtual <virtuals>`` : 
Dans cette action, on distribue une enveloppe dans un dossier de type maildir selon
la table des utilisateurs virtuels. Remarquez que le chemin de la boîte maildir
est précisé de façon à correspondre au dossier indiqué plus haut. 


Enfin, on regarde si on doit envoyer un message ou le délivrer pour appliquer
les actions définies. 

**Notez** que si rien n'est précisé, on considère que la
règle s'applique pour un message venant du serveur : le ``from local`` est
sous-entendu. Sinon, ``from any`` permet d'envoyer un message à partir de votre
ordinateur, en passant par le serveur.

- ``match for local action local_mbox`` : on délivre les messages système.
- ``match from any for domain &NDD action virtual_maildir`` : si le message
  vient de l'extérieur et est pour le nom de domaine du serveur, on le distribue
  aux utilisateurs virtuels.
- ``match auth from any for any action "envoi"`` : lorsqu'un message provient
  d'un client extérieur au serveur **et** authentifié avec son mot de passe
  (avec votre client de messagerie par exemple), on envoie le message.
- ``match for any action "envoi"`` : si les messages provenant de l'intérieur du
  serveur doivent en sortie, on les envoie.



Nous passons maintenant à une étape simple mais importante afin que les mails
soient correctement émis. Il faut indiquer dans le fichier
``/etc/myname`` votre nom de domaine sur une seule ligne. Il s'agit du domaine
que vous avez indiqué dans le champ MX de votre zone DNS : 

```
&NDD
```

Nous pouvons maintenant activer et relancer le serveur //smtpd// : 

```
# rcctl enable smtpd
# rcctl restart smtpd
```

Voilà pour opensmtpd :).


===Dovecot pour l'IMAP===[dovecot]
Dovecot va être utilisé comme serveur IMAP, afin de pouvoir récupérer
son courrier à partir d'un client comme Thunderbird.

On installe dovecot comme d'habitude : 
```
# pkg_add dovecot
```

On édite maintenant le fichier ``/etc/dovecot/local.conf`` pour y mettre
le contenu suivant : 

```
# On écoute en IPV4 et IPV6.
listen = *, [::]

# imap > pop
protocols = imap

# Securisation. Editez ces lignes
ssl = yes
ssl_cert = <&SSLCERT
ssl_key = <&SSLKEY
disable_plaintext_auth = yes

# tres important comme on a modifie les permissions
# sur /etc/mail/passwd
service auth {
    user = $default_internal_user
    group = _maildaemons
}

# methodes d'authentification
passdb {
    args = scheme=blf-crypt /etc/mail/passwd
    driver = passwd-file
}

# Les messages sont dans /var/vmail et appartiennent à _vmail
userdb {
    driver = static
    args = uid=_vmail gid=_vmail home=/var/vmail/%d/%n/ 
}

```

L'exemple ci-dessus est commenté pour vous aider à comprendre ce qui y est fait.

Pensez à adapter l'emplacement des certificats aux variables ``ssl_cert`` et
``ssl_key``. Chaque section est commentée pour que vous compreniez à quoi elles
servent.

Par ailleurs, une configuration ssl est déjà pré-configurée dans le fichier
``/etc/dovecot/conf.d/10-ssl.conf``. C'est censé nous faciliter la vie avec un
script qui génère un certificat auto-signé, mais
comme on a déjà nos certificats et configuré cette partie, il risque de ne pas trop aimer. Dans ce
fichier, commentez donc toutes les lignes restantes : 

```
## Fichier /etc/dovecot/conf.d/10-ssl.conf
#ssl_cert = </etc/ssl/dovecotcert.pem
#ssl_key = </etc/ssl/private/dovecot.pem
```

Afin que dovecot fonctionne correctement, il faut maintenant éditer le
fichier ``/etc/login.conf`` pour ajouter quelques lignes : 
(voir le fichier
``/usr/local/share/doc/pkg-readmes/dovecot*``) 

```
dovecot:\
    :openfiles-cur=512:\
    :openfiles-max=2048:\
    :tc=daemon:
```

On prend en compte les changements récents sur ce fichier avec la commande suivante : 

```
# [ -f /etc/login.conf.db ] && cap_mkdb /etc/login.conf
```

Pour terminer cette partie, on active dovecot et on relance les
différents éléments constituant le serveur mail.

```
# rcctl enable dovecot
# rcctl start dovecot
# rcctl restart smtpd
```

Il vous est désormais possible d'utiliser un client de messagerie afin de
consulter votre courrier.


===Ajouter un nouveau compte mail===
Vous devez remplir les fichiers ``/etc/mail/virtuals`` et ``/etc/mail/passwd``
avec une ligne en plus. Ensuite, lancez les commandes suivantes pour que smtpd
prenne vos changements en compte : 

```
smtpctl update table virtuals
smtpctl update table passwd
```

Ou alors, relancez smtpd avec ``rcctl``.


==Redirection de mail==[mailalias]
Il est simplissime de transférer les mails reçus sur une adresse vers
un autre compte de messagerie.

Par exemple, vous disposez d'une adresse 
``bibi@&NDD`` et souhaitez que tous les messages reçus par bibi 
soient transférés automatiquement à ``jean-eudes@ouaf.xyz``.

Pour ça, il suffit d'éditer le fichier ``/etc/mail/virtuals``, puis d'y
ajouter une ligne comme celle-ci : 

```
bibi&NDD: jean-eudes@ouaf.xyz
```


Dans le fichier ``/etc/mail/aliases``, 
vous pouvez aussi faire des redirections avec les utilisateurs du serveur, très utile pour
l'adminstration système. On peut dans ce cas ne spécifier que le nom
d'utilisateur.

```
root: jean
hostmaster: root
postmaster: root
webmaster:  arthur
jean: toto@serveurmail.net
```

De façon générale, ça donne : 

```
utilisateur: adresse1.mail.com, adresse2.mail.com
```

Afin que ce changement soit pris en compte, il reste 
à lancer la commande suivante :  

```
# rcctl restart smtpd
```

C'est tout! Je vous l'avais dit que c'était simple.



==Configurer son client de messagerie==[mailclient]

Pour consulter vos mails sur le serveur, vous pouvez utiliser un client
de messagerie comme l'excellent [Thunderbird https://www.mozilla.org/fr/thunderbird/], 
logiciel-libre respectueux de
votre vie privée.

Voici les paramètres qu'il faudra indiquer au logiciel pour envoyer et
recevoir des courriels. Notez que tous ne vous seront peut-être pas
demandés : 

- Nom du serveur : ``&NDD``
- Adresse électronique : ``toto@&NDD``
- Nom d'utilisateur : l'adresse mail complète indiquée dans
  ``/etc/mail/virtuals``
- Serveur entrant : IMAP 
    - port : 993
    - SSL : SSL/TLS
- Serveur sortant : SMTP 
    - port : 587
    - SSL : STARTTLS


Vous souhaiterez peut-être plutôt utiliser un [webmail #webmail], afin d'accéder
à votre messagerie à partir d'un navigateur web. 


===Enregistrements DNS facultatifs pour clients===
Pour simplifier la configuration des clients de messagerie, vous pouvez ajouter
dans votre zone DNS les enregistrements suivants, compris par la plupart des
logiciels existants : 


```
_submission._tcp.example.com	86400	IN	SRV	0 1 587	&NDD
_imap._tcp.example.com		86400	IN	SRV	0 0 0	.
_imaps._tcp.example.com		86400	IN	SRV	0 1 993	&NDD
```


==Ne pas perdre de messages : MX==[mxbackup]
Il est possible que des mails n'arrive pas à votre serveur si votre serveur est
hors-ligne pendant plus d'une semaine. Normalement, tous les serveurs de
messagerie tenteront de renvoyer un mail qui n'a pas pu être délivré.

Cependant, c'est plus rassurant de prévoir le coup en configurant une solution
de secours. Il vous faut pour cela : 

- Un second serveur (ahem... :S)
- Ou un ami avec un serveur :)


Ce serveur gardera vos messages en file d'attente le temps que le vôtre
redevienne disponible.

De votre côté, il vous suffit d'ajouter à [votre zone #DNS] un nouveau champ MX
avec un "poids" plus élevé que le vôtre. Ce champ pointe vers le serveur
secondaire : 


```
@                     IN MX 10  &NDD.
@                     IN MX 70  mail.copain.eu.
```

Dans l'exemple ci-dessus, votre serveur a un poids inférieur (10) que celui de
du serveur secondaire (70). Ce dernier sera utilisé si le premier n'est pas
disponible.


De son côté, votre ami, pour peu qu'il utilise lui aussi opensmtpd n'aura qu'à
ajouter les lignes suivantes à son ``/etc/mail/smtpd.conf`` afin de se
constituer serveur mail de secours : 

```
action relaybackup relay backup helo "&NDD"
...
match from any for domain &NDD action relaybackup
```


L'idéal, c'est de lui rendre la pareille ;).

==Ne pas être mis dans les spams==
En l'état, vous pouvez recevoir et envoyer des messages. Cependant, il se peut
que certains serveurs de messagerie considèrent vos mails comme des spams.
Heureusement, il existe quelques petites manipulations pour rendre vos messages
légitimes. Nous allons les détailler dans les paragraphes suivants. Gardez à
l'esprit qu'elles se complètent sans se suffire à elles-mêmes.


===Reverse DNS===[reverseDNS]
Chez votre Fournisseur d'Accès à internet, cherchez les options correspondant à
votre adresse IP. Vous pourrez configurer un //reverse DNS// ou en
français //DNS inverse//. 

Alors que votre nom de domaine est relié à l'IP du serveur, il faut aussi
configurer la réciproque, c'est-à-dire relier à votre IP le nom de domaine.

Un petit mail à votre fournisseur permettra de savoir comment s'y prendre
si vous ne trouvez pas ;).

Si vous ne pouvez pas faire cette manipulation, ce n'est pas une catastrophe, du
moment que vous pouvez mettre en place SPF et DKIM (voir paragraphes suivants).

===SPF===

Ajoutez un champ DNS de type SPF dans votre zone DNS qui correspond au champ A
utilisé pour vos mails (chez votre registrar ou
sur votre [serveur de noms #serveur-de-nom] si vous l'hébergez aussi) tel que celui-ci : 
```
&NDD.   SPF "v=spf1 a mx ~all"
```

ou bien sous forme de champ TXT si le SPF n'est pas disponible : 
```
&NDD. TXT "v=spf1 a mx ~all"
```

===Signature DKIM===[DKIM]
Cette technique consiste à signer les messages émis par le serveur à l'aide
d'une clé privée. On ajoute ensuite dans un champ DNS la clé publique
correspondante qui
permettra au destinataire de vérifier que le mail reçu provient bien de votre
serveur.


	Moi pas compris!


Je reprends. Nous allons créer une clé //privée// et une clé //publique//.

La clé //privée// servira à signer les messages. On
l'appelle "**privée**" car vous devez être la seule personne capable de signer
(comme pour vos chèques ou vos impôts).
La clé //publique// est là pour vérifier que la signature est bien authentique. On
peut voir ça comme une pièce de puzzle unique, qui est la seule à pouvoir entrer
dans l'empreinte créée par la signature.

On installe dkimproxy comme d'habitude : 

```
# pkg_add dkimproxy
```

====Génération des clefs OpenSSL====


Les commandes suivantes permettent de fabriquer la paire de clés qui servira à
signer les mails émis :

+ Création du dossier pour les clefs
```
# mkdir -p /etc/dkimproxy/private                    
```

+ On protège le dossier
```
# chown _dkimproxy:wheel /etc/dkimproxy/private 
```

+ On modifie les permissions
```
# chmod 700 /etc/dkimproxy/private                   
```

+ On va dans le dossier
```
# cd /etc/dkimproxy/private                          
```

+ On génère les clefs
```
# openssl genrsa -out private.key 1024               
# openssl rsa -in private.key -pubout -out public.key
```

+ On protège les clefs
```
# chown _dkimproxy:wheel private.key public.key      
# chmod 400 private.key
```


====Configuration DKIM====


Afin de configurer la signature des messages envoyés, il faut éditer le ficher
``/etc/dkimproxy_out.conf`` ainsi : 

```
listen    127.0.0.1:10027
relay     127.0.0.1:10028
domain    &NDD
signature dkim(c=relaxed)
signature domainkeys(c=nofws)
keyfile   /etc/dkimproxy/private/private.key
selector  pubkey
```

	Euh, c'est quoi tout ça?

Quelques menues explications : 

- Les lignes ``listen`` et ``relay`` indiquent l'adresse avec le port 
  où dkimproxy recevra respectivement les mails à chiffrer et où il les fera
  suivre. Tout se passe dans votre serveur, donc à l'adresse ``127.0.0.1``.
- ``domain`` décrit votre nom de domaine : à modifier selon votre cas. 
- ``keyfile`` permet d'indiquer où se trouve le chemin vers la clé servant à
  signer les mails. 
- ``selector`` définit l'identifiant qui sera présent dans le champ DNS que l'on
  va définir ensuite.


Bien, reste à indiquer à opensmtpd de signer les mails. On va donc ajouter dans
le fichier ``/etc/mail/smtpd.conf`` une ligne pour écouter sur le port 10028 en
local, afin d'envoyer les mails que dkimproxy aura signé. On leur colle
l'étiquette "DKIM" pour les repérer ensuite.

```
listen on lo0 port 10028 tag DKIM   
```

Les messages qui auront l'étiquette "DKIM" peuvent être envoyés. On n'envoie
plus les mails s'ils ne sont pas passés par dkimproxy. 

```
match tag DKIM for any action "envoi"
match auth tag DKIM from any for any action "envoi"
```

Enfin, les messages pas encore signés doivent être envoyés à dkimproxy. On
définit l'action correspondante : 

```
action dkimproxy relay host smtp://127.0.0.1:10027 
```


S'ensuit les règles de correspondance qui vont bien, à placer à la fin du
fichier ``smtpd.conf``:

```
match auth from any for any action dkimproxy
match for any action dkimproxy
```

Le fichier ``/etc/mail/smtpd.conf`` ressemble désormais à ça : 

```
# Configuration generale
## Tables 
table aliases file:/etc/mail/aliases
table passwd passwd:/etc/mail/passwd
table virtuals file:/etc/mail/virtuals

## Certificats
pki &NDD key "&SSLKEY"
pki &NDD cert "&SSLCERT"

### Ecoute pour messages signes avec dkimproxy
listen on lo0 port 10028 tag DKIM   
### Messages locaux
listen on lo0 

### Reception
listen on egress tls pki &NDD 
### Envoi avec client de messagerie
listen on egress port submission tls-require pki &NDD auth <passwd> 


# ACTIONS
action "envoi" relay 
action dkimproxy relay host smtp://127.0.0.1:10027 
action spamassassin relay host smtp://127.0.0.1:10025 

action local_mbox mbox alias <aliases>
action virtual_maildir maildir "/var/vmail/%{dest.domain}/%{dest.user}/Maildir" virtual <virtuals>

# Correspondances
## Reception
### Message pour les utilisateurs locaux qui lisent avec la commande "mail"
match for local action local_mbox
### Message pour les utilisateurs virtuels
match from any for domain "&NDD" action virtual_maildir

## Envoi
### Mail sortant portant une signature DKIM
match tag DKIM for any action "envoi"
match auth tag DKIM from any for any action "envoi"

### Mail en envoi pas encore signe avec DKIM
match auth from any for any action dkimproxy
match for any action dkimproxy
```

Ça va? Vous suivez toujours? Je vois à votre regard pétillant que vous attendez
la fin avec impatience ! ^^

Courage, c'est presque fini. ;) 


====Enregistrements DNS====


Pour terminer, nous allons ajouter un nouveau champ dans vos DNS auprès de votre
registrar ou dans votre zone. Eh oui, encore ! On va en réalité indiquer le nécessaire pour pouvoir
vérifier la signature des messages qui auront un drapeau "pubkey".

Il s'agira d'un champ DKIM ou TXT selon ce qui est disponible.
Remplissez-le ainsi :  
- Nom de domaine : ``pubkey._domainkey``.
- Contenu : ``v=DKIM1; k=rsa; p=...``
Recopiez à la place des points de suspension le contenu du fichier
``public.key``, que vous pouvez afficher avec la commande
``cat`` :

```
# cat /etc/dkimproxy/private/public.key
```

Ce qui nous donnera dans la zone DNS votre domaine : 

```
pubkey._domainkey    IN TXT    ( "v=DKIM1; k=rsa; t=s;p=v+Fb...vhP/oB")
```


====Gestion du service DKIM====


Finalement, activez dkimproxy puis rechargez opensmtpd avant de
tester si vous avez réussi à configurer correctement l'envoi de vos mails.

```
# rcctl enable dkimproxy_out
# rcctl start dkimproxy_out
# rcctl restart smtpd
```


====Vérifications====

Pour vérifier que vos messages envoyés ne sont pas considérés comme spam, suivez les indications 
du site [mail-tester https://www.mail-tester.com/]. Vous allez envoyer un
message à l'adresse donnée, et normalement, vous devriez obtenir au moins une
note de 8/10 : 

[img/mail-tester1.png]

[img/mail-tester2.png]

Il se peut qu'on vous parle d'un enregistrement dmarc. Libre à vous de l'ajouter
à vos DNS, mais ce n'est pas obligatoire. À vrai dire, le score obtenu est déjà
meilleur qu'avec nombre de "grands" services de messagerie (testez avec gmail
pour rigoler : 6.1/10 lors de mon dernier test...).


==Utiliser un relai SMTP externe==[remotesmtp]

Si votre fournisseur d'accès à internet bloque le port smtp (25), vous serez
bien embêtés pour envoyer des messages. Deux solutions s'offrent à vous : 

- Changer de FAI ^^
- Utiliser un relai smtp externe avec identification. Ce dernier peut relayer
  les messages envoyés par votre serveur.
C'est bien évidemment cette situation que nous allons détailler, notamment grâce
à l'aide de l'article de 
[PengouinBSD https://blog.stephane-huc.net/systeme/openbsd/config-smtpd].



Nous supposerons que vous disposez d'un accès par smtp sur un autre serveur
(votre fournisseur de mail habituel). Vous envoyez déjà des courriels avec ce
dernier à l'aide d'un identifiant et d'un mot de passe. Mettez ces derniers dans
un fichier ``/etc/mail/secrets`` sous cette forme : 

```
# echo "id_secret utilisateur:motdepasse" > /etc/mail/secrets
```


Bien sûr, vous aurez remplacé les éléments suivants : 

- "utilisateur" : Nom d'utilisateur à votre compte mail habituel. C'est souvent
  votre adresse mail. 
- "motdepasse" : Mot de passe pour vous connecter à votre compte mail habituel. 
- "id_secret" : Il s'agit du nom que vous voulez donner à ce relai. C'est un
  **repère**. On se
  servira de ce nom ensuite pour désigner ce relai afin de ne pas avoir à
  enregistrer vos identifiants secrets dans le fichier ``/etc/mail/smtpd.conf``
  qui est lisible par tous les utilisateurs du système.


Avant d'aller plus loin, modifiez les [permissions #permissions] sur ce fichier afin que seul
**smtpd** puisse le lire et **root** le modifier : 

```
# chmod 640 /etc/mail/secrets
# chown root:_smtpd /etc/mail/secrets
```


Ensuite, modifiez le ficher ``/etc/mail/smtpd.conf`` comme ceci afin d'indiquer
d'envoyer les messages à l'aide de ce service externe : 

```
...
table secrets file:/etc/mail/secrets
...
listen on lo0
...

action "relay" relay host smtp+tls://id_secret@smtp.exemple.com \
		auth <secrets> mail-from "@&NDD"

...
match from any for any action "relay"
```


Un peu d'explications ne feront pas de mal, surtout pour que vous sachiez quoi
modifier : 

- ``table secrets`` : On indique où se situe le fichier contenant les
  identifiants secrets pour joindre le relai smtp définit plus haut.
- ``action "relay" relay ...`` : on définit l'action qui permet
  d'envoyer les mails par le biais du serveur smtp externe qui sert de relai.
  Il faut ici modifier "``id_secret``" par le repère définit plus haut. Ainsi,
  on n'écrit pas directement vos identifiants, c'est plus sûr puisque le fichier
  ``/etc/mail/smtpd.conf`` est lisible par tous les utilisateurs de votre
  serveur.
  Vous devez aussi changer le nom de domaine du serveur relai
  ``smtp.exemple.com``.
- ``smtp+tls`` : il s'agit du protocole utilisé pour communiquer avec le relai.
  Ça peut être ``smtp`` pour une session smtp normale avec STARTTLS si possible,
  ``smtp+tls`` pour une utilisation de STARTTLS obligatoire, ``smtp+notls`` si
  l'identification n'est pas chiffrée ou bien ``smtps`` pour une connexion SSL.
  À vous de choisir ce que votre fournisseur propose (espérons ``smtp+tls`` qui
  est plus sûr).
- ``auth <secrets>`` : on utilise la table contenant les identifiants secrets.
- ``mail-from "@&NDD"`` : On précise le nom de domaine de votre serveur pour que
  le courriel soit bien marqué comme venant de **votre serveur** et non du
  relai.


Pour finir, rechargez ``smtpd`` pour profiter de cette nouvelle configuration : 

```
rcctl restart smtpd
```


Pour un autre exemple, vous pouvez consulter 
[le fil de discussion sur obsd4a.net https://obsd4a.net/forum/showthread.php?tid=2114] 
ou encore le
[manuel de configuration pour smtpd https://man.openbsd.org/smtpd.conf].




==Installer un antispam==

Votre serveur n'est actuellement pas à l'abri des spams. Il existe plusieurs
façons pour lutter contre ces nuisances. Nous allons commencer par étudier
une méthode simple avec [spamassassin #spamassassin] pour s'en protéger.

%Vous
%pouvez aussi utiliser l'alternative tout aussi efficace [rspamd #rspamd].

Notez qu'il existe des
techniques plus efficaces mais plus difficiles à mettre en place
puisque les gros fournisseurs de messagerie disposent de nombreuses adresses IP.
Pour ceux que le sujet intéresse, vous pouvez consulter la documentation de
spamd et de [bgpd http://bgp-spamd.net/client/]. Nous en parlerons rapidement
dans un [second temps #spamd].

D'autres programmes très complets pour lutter contre le spam existent comme 
[rspamd https://www.rspamd.com/] si vous souhaitez vous renseigner.

	Houla, ça fait beaucoup pour faire la même chose !

Pour ma part, je trouve rassurant de lire que l'équipe OpenBSD utilise
[spamassasin #spamassassin] et [spamd #spamd] pour protéger ses 
[listes de diffusion https://www.openbsd.org/mail.html]. 

Ça tombe bien, ce sont
les deux outils détaillés dans ce manuel ^^.



===Spamassassin===[spamassassin]
%!include: spamassassin.t2t

%== Antispam avec rspamd ==[rspamd]
%%!include: rspamd.t2t

===Gestion des spams et dovecot===[sieve]
%!include: sieve.t2t

=== Antispam avec spamd ===[spamd]
%!include: spamd.t2t



==Vérifier que tout fonctionne bien==

Vous voudrez peut-être tester votre serveur mail après tous ces efforts. Vous
l'avez bien mérité. Vous pouvez bien entendu écrire à des amis, mais cela peut
poser des soucis : 

- Il faudra attendre leur réponse ; 
- Ils ne vous retourneront pas toutes les informations dont vous pourriez avoir
  besoin ;
- Il faut avoir des amis.


Il existe heureusement des robots auxquels on peut écrire un mail, qui vous
répondront très vite en vous donnant de précieuses informations. Nous avons déjà
parlé de [mail-tester http://www.mail-tester.com] un peu plus haut. Il existe aussi les adresses
suivantes : 

- ``echo@generic-nic.net`` 
- ``echo@nic.fr``
- ``autoreply@dmarctest.org``
- Le site https://glockapps.com/ permet de tester sa réputation.


Vous en trouverez d'autres sur 
[cette page http://www.bortzmeyer.org/repondeurs-courrier-test.html].


Vous pouvez aussi avoir des tests détaillés sur votre serveur mail est ses
backup éventuels sur [mxtoolbox https://mxtoolbox.com/].

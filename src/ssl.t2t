Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8


==Les certificats SSL==[sslcert]

Vous avez peut-être déjà lu la solution utilisant letsencrypt avec 
[acme-client #acmeclient]. Ce paragraphe propose quelques autres informations
sur les certificats SSL.

===Générer un certificat SSL auto-signé===
Nous allons ici auto-signer le
certificat. Les visiteurs de votre site risquent juste d'avoir un
avertissement de ce type : 

[img/avertissement-ssl.png]

Sachez qu'il est possible d'acheter une autorité de certification.  Mais
dépenser votre argent n'est pas nécessaire n'est-ce pas? De plus un
certificat auto-signé ne retire en rien la protection du chiffrement
SSL.

Pour créer un certificat et le signer, il faut lancer la
commande suivante. Bien sûr, remplacez le nom du fichier
``certificat`` à votre convenance : 

```
# openssl req -x509 -sha512 -nodes -days 365 -newkey rsa:4096 \
	-keyout /etc/ssl/certificat.key \
	-out /etc/ssl/private/certificat.pem
```

Quelques questions vous seront posées. Vous n'êtes pas obligé de
remplir tous les champs.

Finalement, il faut protéger ce certificat. Lancez ces
deux dernières commandes afin d'en restreindre les permissions : 

```
# chown root:wheel /etc/ssl/private/certificat.key
# chmod 600 /etc/ssl/private/certificat.pem
```

Retenez bien le chemin vers ce certificat. Il faudra le préciser dans
la configuration de votre serveur web (http).

===Tester la sécurité SSL===

Les plus intéressés pourront tester la sécurité de leur serveur sur 
[le site sslabs https://www.ssllabs.com/], qui fourni
d'excellents conseils pour l'améliorer.

[img/ssllabs.png]

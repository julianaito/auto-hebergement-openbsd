Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

==Installation chiffrée==[diskcrypt]

Si un jour votre serveur est volé par un méchant cambrioleur, vos données
peuvent rester tranquilles si vous chiffrez l'ensemble de votre système.
Tout est bien pensé par OpenBSD qui a décrit la procédure dans 
[sa FAQ https://www.openbsd.org/faq/faq14.html#softraidFDE].
Notez que vous devrez être en mesure d'entrer la phrase de déchiffrement du
disque à chaque démarrage et redémarrage du serveur, ce qui peut être une
contrainte pour certains.

En résumé, juste avant l'installation, vous verrez le message suivant : 

```
Welcome to the OpenBSD/amd64 X.X installation program.
(I)nstall, (U)pgrade, (A)utoinstall or (S)hell? s
```


Choisissez l'invite de commandes avec "S".

À partir de ce moment-là, on remplit le disque de zeros pour écraser toutes les
données pré-existantes (``sd0`` correspond à votre disque dur ici): 

```
dd if=/dev/urandom of=/dev/rsd0c bs=1m
```

Ça peut être long selon le matériel (taille et vitesse d'écriture).

Ensuite, on crée une partition softraid qui sera chiffrée : 


- Si vous démarrez via MBR (défaut) : ``# fdisk -iy sd0``
- Si vous démarrer avec GPT ou UEFI : ``# fdisk -iy -g -b 960 sd0``

Créez la table de partition sur ``sd0`` avec un type ``RAID``: 

```
disklabel -E sd0
Label editor (enter '?' for help at any prompt)
> a a			
offset: [64]
size: [39825135] *
FS type: [4.2BSD] RAID
> w
> q
No label changes.
```


Ensuite, on peut créer le périphérique chiffré avec une bonne
[phrase de passe #mdp] : 

```
# bioctl -c C -l sd0a softraid0
New passphrase:
Re-type passphrase:
sd1 at scsibus2 targ 1 lun 0: <OPENBSD, SR CRYPTO, 005> SCSI2 0/direct fixed
sd1: 19445MB, 512 bytes/sector, 39824607 sectors
softraid0: CRYPTO volume attached as sd1
```


Cela crée un pseudo-périphérique ``sd1``. Pour être sûr que l'installateur le prenne en
compte, lancez : 

```
# cd /dev && sh MAKEDEV sd1
```


On écrase les premières données du pseudo-périphérique : 
```
# dd if=/dev/zero of=/dev/rsd1c bs=1m count=1
```

Enfin, entrez ``exit`` pour retrouver l'installateur, et procédez à
l'installation sur ce pseudo-périphérique ``sd1`` tout naturellement.

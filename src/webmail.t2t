Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4



===Un Webmail===[webmail]
Le webmail vous servira à consulter votre messagerie à partir d'un navigateur
web. 

====RainLoop====[rainloop]
[RainLoop https://www.rainloop.net/] est un excellent webmail qui est
facile à installer et à mettre à jour. Il permet non seulement de consulter les
messages présents sur votre serveur mais aussi ceux présents chez d'autres
hébergeurs, un peu comme le fait 
[Thunderbird https://www.mozilla.org/en-US/thunderbird/]. De plus, il intègre
par défaut un support pour le chiffrement PGP, bien que partiel.

Pour que RainLoop fonctionne correctement, vous devrez installer l'extension
``php-curl`` et l'activer comme [décrit plus haut #phppp].


Nous allons mettre RainLoop dans un dossier ``/var/www/htdocs/webmail`` que nous
allons créer et dans lequel nous nous plaçons :  

```
# mkdir -p /var/www/htdocs/webmail
# cd /var/www/htdocs/webmail
```


On télécharge l'archive puis on la décompresse : 

```
# ftp "https://www.rainloop.net/repository/webmail/rainloop-community-latest.zip"
# unzip rainloop*.zip
```


Afin d'attribuer des permissions raisonnables aux fichiers de RainLoop, on
exécute les commandes suivantes : 

```
find . -type d -exec chmod 755 {} \;
find . -type f -exec chmod 644 {} \;
chown -R www:daemon .
```


Il ne nous reste plus qu'à configurer httpd. Comme d'habitude, on rajoute dans
le fichier ``/etc/httpd.conf`` une nouvelle section : 

```
server "webmail.&NDD" {
        listen on * tls port 443
        root "/htdocs/webmail"
        directory index index.php
        # 35M maxi, la valeur par défaut de smtpd
        connection max request body 36700160
        hsts
        tls {
            certificate "&SSLCERT"
            key "&SSLKEY"
        }
        location "/data*"                { block }

        location "/*.php*" {
                fastcgi socket "/run/php-fpm.sock"
        }
}
```


On recharge httpd : ``rcctl reload httpd`` puis on ouvre dans un navigateur  la
page d'administration du webmail située à l'adresse suivante : ``https:/&NDD/?admin``.

Par défaut, le login administrateur est ``admin`` et le mot de passe ``12345``.
**Changez-les tout de suite**.


Pour la gestion des pièces jointes, qui est maximum de 35M par défaut avec le
serveur mail smtpd, vous devriez changer les valeurs suivantes dans la 
[configuration avancée de php #phppp].

```
post_max_size = 35M
upload_max_filesize = 35M
```

=====Configuration de RainLoop=====
RainLoop permet de consulter des messages provenant de serveurs différents.
La configuration se déroule à l'adresse ``&NDD/?admin``.
Ainsi, si vous vous dirigez dans l'onglet "Domains", vous pouvez en voir
plusieurs pré-configurés.

Nous ajoutons un nouveau domaine (le vôtre) en cliquant sur "+ Add Domain".


[img/rainloop-config1.png]


Une fenêtre s'ouvre. Complétez le champ "Name" avec le nom de domaine de votre
serveur.
Pour la configuration IMAP et SMTP, vous pourriez réaliser la même configuration
que pour n'importe quel [client #mailclient]. J'indique ici une configuration
qui fonctionnera avec la mise en place d'un serveur mail telle que décrite dans
le présent document.

[img/rainloop-config2.png]

Cliquez sur "Test" Afin de vérifier que tout fonctionne comme prévu.

[img/rainloop-config3.png]


Dirigez-vous maintenant vers la page principale (sans le ``?admin``) de votre webmail pour vous y
connecter. **Attention**, il faut rentrer votre adresse mail complète comme
login : 


[img/rainloop.png]



====Roundcubemail====
Nous allons ici installer le très connu [roundcube https://roundcube.net/].
Cette application est complète, toutefois relativement complexe à installer.
Vous voudrez peut-être installer le paquet ``roundcubemail`` déjà tout prêt,
mais peut-être à une version plus ancienne, et lire le fichier
``/usr/local/share/doc/pkg-readmes/roundcubemail``.


Installons tout d'abord quelques dépendances  qu'il faudra [activer #phppp]: 

```
# pkg_add sqlite3
# pkg_add php-pspell-&PHPVER php-zip-&PHPVER libmcrypt
# pkg_add php-intl-&PHPVER 
```


On doit ensuite modifier la configuration de PHP. On édite le fichier
``/etc/php-&PHPMINVER.ini`` pour y mettre à la fin : 

```
[suhosin]
suhosin.session.encrypt = 0
```

Une fois cette modification effectuée, relancez PHP : 

```
# rcctl enable php&PHPMICROVER_fpm
# rcctl restart php&PHPMICROVER_fpm
```

On va mettre roundcube dans le dossier ``/var/www/htdocs/roundcube``.


On télécharge l'archive de roundcube qu'on décompresse : 

```
# cd /var/www/htdocs/
# ftp -o roundcube.tgz https://github.com/roundcube/roundcubemail/releases/download/1.3.8/roundcubemail-1.3.8-complete.tar.gz
# tar xvzf roundcube.tgz
```

Maintenant, on renomme le nouveau dossier ``roundcubemail*`` puis on crée les
dossiers nécessaires au bon fonctionnement de roundcube : 

```
# mv roundcubemail-* roundcube
# mkdir -p roundcube/temp roundcube/logs
```

Nous allons créer la base sqlite pour roundcube.
On fabrique un dossier
qui contiendra la base de données : 

```
# mkdir /var/www/htdocs/roundcube/db
```

La commande suivante crée la base : 

```
# cd /var/www/htdocs/roundcube
# sqlite3 -init SQL/sqlite.initial.sql db/sqlite.db
-- Loading resources from roundcube/SQL/sqlite.initial.sql
SQLite version 3.24.0 2018-06-04 19:24:41
Enter ".help" for usage hints.

(Tapez .exit pour quitter sqlite3)
```

Enfin, on modifie les permissions de tous ces nouveaux fichiers : 

```
# cd /var/www/htdocs/
# chown -R www:daemon roundcube
# chmod 0775 roundcube/db
# chmod 0660 roundcube/db/sqlite.db
```

On ajoute le nouveau site dans la configuration de //httpd//. Pour cela, on édite le
fichier ``/etc/httpd.conf`` et on ajoute quelque chose comme : 

```
server "webmail.&NDD" {
        listen on * port 80
        block return 301 "https://$SERVER_NAME$REQUEST_URI"
        no log
}

server "webmail.&NDD" { 
        listen on * tls port 443 
        root "/htdocs/roundcube" 
        directory index index.php
        no log

        hsts 
        tls {
            certificate "&SSLCERT"
            key "&SSLKEY"
        }
        location "*.php*" {
            fastcgi socket "/run/php-fpm.sock"
        }
        # Deny Protected directories
        location "/config*" { block }
        location "/temp*" { block }
        location "/logs*" { block }
        location "/README" { block }
        location "/INSTALL" { block }
        location "/LICENSE" { block }
        location "/CHANGELOG" { block }
        location "/UPGRADING" { block }
        location "/bin*" { block }
        location "/SQL*" { block }
        location "/db*" { block }
        location "*.md" { block }
        location "\.*" { block }
} 
```

Rechargez //httpd// et PHP puis allez à la page d'installation de votre nouveau
webmail avec un navigateur : ``https://webmail.&NDD/installer``.

Suivez les indications données. La plupart des choses n'ont pas besoin d'être
modifiées. Vérifiez tout de même que : 

- Pour la base de données, vous choisissez SQLite.
- Le nom de la base de données (Database name) doit être celui-ci (avec tous les
  ``/``...):

  ``////htdocs/roundcube/db/sqlite.db``

- Les autres champs pour la base de données doivent être vides.
- Pour ``smtp_server``, la valeur doit être ``localhost``.


Dans le navigateur sera générée la
configuration. Enregistrez-la dans le fichier
```
/var/www/roundcubemail/config/config.inc.php
```
Vérifiez bien qu'il contient au moins ceci (attention au nombre de
``/``) : 

```
$config['db_dsnw'] = 'sqlite:////htdocs/roundcube/db/sqlite.db?mode=0660';
$config['smtp_server'] = 'localhost';
```

Vous avez une dernière page de test, puis vous pouvez allez à l'URL de votre
webmail pour voir que tout fonctionne.


Bien que tout semble être en état de marche, n'oublions pas la
sécurité.
Modifiez le fichier ``config.inc.php`` pour
désactiver l'installateur.

```
$config['enable_installer'] = false;
```

Puis supprimez le dossier d'installation totalement : 

```
# rm -r /var/www/htdocs/roundcube/installer
```


Ça y est, votre webmail est prêt!

[img/roundcube.png]



À l'avenir, pour mettre roundcube à jour, lisez le fichier ``UPGRADING`` et la
section ``Update manually``.


Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%c)

%! encoding: utf-8


===Exemple d'installation détaillée d'OpenBSD===[install]

Cette section n'est là que pour rassurer les plus réticents. Pour des
informations plus complètes, n'hésitez pas à consulter 
la [documentation d'OpenBSD https://www.openbsd.org/faq/faq4.html].

On télécharge tout d'abord l'[image d'installation https://www.openbsd.org/faq/faq4.html#Download] de la dernière version d'OpenBSD, qui
est, à l'heure où j'écris ces lignes, la OBSDVERSION.  

N'oubliez pas de vérifier la somme de contrôle. À partir d'un système OpenBSD,
cela se passe avec cette commande : 

```
signify -Cp /etc/signify/openbsd-OBSDMINVERSION-base.pub -x SHA256.sig install*.fs
```

Sinon, sur un autre système, calculez la somme de contrôle "sha256" puis
comparez avec le contenu du fichier ``SHA256`` présent à cette adresse :
https://cdn.openbsd.org/pub/OpenBSD/OBSDVERSION/amd64/ (remplacez
"amd64" par votre architecture ;) ).


- Pour installer à partir d'un cdrom, choisissez installOBSDMINVERSION.iso puis gravez-la sur
  le CD.
- Pour installer à partir d'une clé USB, choisissez installOBSDMINVERSION.fs.  Pour préparer une
  clé USB, utilisez l'outil ``dd`` (remplacez ``/dev/sdb`` par le chemin
  vers votre clé USB). Par exemple sur un système GNU/Linux : 

```
# dd if=/location/install*.fs of=/dev/sdb bs=1M 
```

À partir d'OpenBSD : 

```
# dd if=/location/install*.fs of=/dev/rsd1c bs=1m 
```

La commande ``dd`` va reproduire le contenu de l'image sur la clé USB. Pour
éviter d'avoir à écrire le nom exact de l'image d'installation, on utilise le
symbole ``*`` qui veut dire "n'importe quelle suite de caractères". Les autres
options servent au bon fonctionnement du transfert.


Vous pouvez récupérer un outil graphique nommé [etcher https://etcher.io/] pour
préparer la clé USB plus sereinement si la commande ``dd`` ne vous plaît pas.


Les utilisateurs de Windows pourront faire la même chose avec le logiciel 
[rufus https://rufus.akeo.ie/].
    
Et hop, on insère tout ça dans le lecteur ou prise USB du PC, puis on redémarre en
choisissant de démarrer sur le bon disque (//boot// en anglais). Repérez les messages qui s'affichent du
type ``F12 : Boot Menu`` ou ``F7 : Setup`` indiquant les touches qui permettent de faire
apparaître les fenêtres (bleues) des tableaux de réglage du BIOS. On y indique
souvent le lecteur de CDROM ou la clef USB comme média de démarrage numéro 1.

Si au cours de la configuration de l'installation vous vous trompez, 
**pas de panique**. Appuyez sur
ctrl-C puis entrez la commande ``install`` afin de recommencer la configuration
au début en retrouvant vos choix précédents.

Le premier écran nous propose d'ajouter des options pour démarrer OpenBSD.
Normalement, on n'en a pas besoin, on appuie donc juste sur "Entrée" : 

```
>> OpenBSD/amd64 CDBOOT 341.
boot> 
```


Si votre machine a un port série, vous avez besoin d'un retour console. Vous
devrez alors préciser ces options : 

```
>> OpenBSD/i386 PXEBOOT 1.00
boot> stty com0 19200
boot> set tty com0
boot> <Appuyez sur entree>
```


Plein de texte sur fond bleu apparaît, indiquant la détection du
matériel et autres choses qui ne doivent pas vous inquiéter.

On lance l'installation en entrant "``I``" : 

```
erase ^?, werase ^W, kill ^U, intr ^C, status ^T

Welcome to the OpenBSD/amd64 6.5 installation program.
(I)nstall, (U)pgrade, (A)utoinstall or (S)hell?
```


Les valeurs proposées pour l'installation sont largement suffisantes
dans la plupart des cas. Les choix par défaut sont indiqués entre crochets : 
``[choix]``. À chaque fois, des exemples sont disponibles avec ``'?'``.

On choisit une disposition de clavier : fr 


```
At any prompt except password prompts you can escape to a shell by typing '!'.
Default answers are shown in []'s and are selected by pressing RETURN.  
You can exit this program by pressing Control-C, 
but this can leave your system in an inconsistent state.

Choose your keyboard layout ('?' or 'L' for list) [default] fr
```


On choisit ensuite un nom de machine, par exemple "maitre.&NDD" ou "maitre".

```
System hostname? (short form, e.g. 'foo')
```


Ensuite, on configure la connexion à Internet. Notez qu'il n'est pas obligatoire
d'avoir un accès en ligne si vous avez choisi une image ``install*.*`` qui contient
tous les éléments pour faire l'installation. 

Vous avez la possibilité de définir une [IP statique #IP] pour votre serveur ou bien
utiliser la configuration DHCP par facilité.


```
Available network interfaces are: re0 vlan0.
Which network interface do you wish to configure? (or 'done') [re0]
IPv4 address for re0? (or 'dhcp' or 'none') [dhcp]
DHCPDISCOVER on re0 - interval 1
DHCPOFFER from 10.0.2.2 (52:55:01:00:02:02)
DHCPREQUEST on re0 to 255.255.255.255
DHCPACK from 10.0.2.2 (52:55:01:00:02:02)
bound to 10.0.2.15 -- renewal in 43200 seconds.
```


Pour avoir une IPv6, choisissez ``autoconf`` le moment venu pour en avoir une
automatiquement. Vous souhaiterez certainement configurer cette partie
[manuellement #reseau] par la suite.

```
IPv6 address for re0? (or 'autoconf' or 'none') [none] autoconf
Available network interfaces are: re0 vlan0.
Which network interface do you wish to configure? (or 'done') [done]
```


La configuration réseau se termine par la route IPv4 et le DNS. Laissez les choix par
défaut à moins d'avoir un besoin très particulier : 

```
Default IPv4 route? (IPv4 address or none) [10.0.2.2]
add net default: gateway 10.0.2.2
DNS domain name? (e.g. 'bar.com') [my.domain]
Using DNS nameservers at 10.0.2.3
```


Ensuite, l'installateur vous demande le mot de passe de l'administrateur
système dont le petit nom est //root//. 
Choisissez [un mot de passe robuste #mdp].

```
Password for root account? (will not echo)
Password for root account? (again)
```


Vous pouvez ensuite faire en sorte que [SSH #ssh] soit lancé par défaut au démarrage.
C'est conseillé pour un serveur afin de pouvoir l'administrer sans écran à
partir d'un autre ordinateur.

On nous demande si un serveur X sera utilisé (session graphique) : ce n'est
absolument pas nécessaire pour un serveur. Autant attribuer toutes les
ressources aux calculs autres que l'affichage.

Vous ne voudrez sans doute pas changer la console par défaut, laissez "no".

```
Password for root account? (will not echo)
Password for root account? (again)
Start sshd(8) by default? [yes]
Do you expect to run the X Window System? [yes] no
Change the default console to com0? [no]
```


Vous pouvez [ajouter un utilisateur #adduser] si vous le souhaitez en entrant son login.
Vous n'êtes pas obligés, mais je vous le conseille pour vous connecter à votre
serveur via SSH. Ainsi, vous éviterez d'utiliser le compte root.

```
Setup a user? (enter a lower-case loginname, or 'no') [no] luffy
Full name for user luffy? [luffy] Mugiwara No Luffy
Password for user luffy? (will not echo)
Password for user luffy? (again)
```


On vous demande ensuite si vous voulez autoriser l'utilisateur "root" à se
connecter via SSH. **C'est une très mauvaise idée**, puisque cet utilisateur a
tous les droits et est la cible privilégiée des pirates. Choisissez "no".

```
WARNING: root is targeted by password guessing attacks, pubkeys are safer.
Allow root ssh login? (yes, no, prohibit-password) [no]
```


Le fuseau horaire peut être modifié : 

```
What timezone are you in? ('?' for list) [Canada/Mountain] Europe/Paris
```


C'est parti pour le choix du disque sur lequel installer OpenBSD : 

```
Which disk is the root disk? ('?' for details) [wd0]
Use (W)hole disk MBR, whole disk (G)PT, (O)penBSD area or (E)dit? [whole]
```


Après avoir appuyé sur Entrée, vous voyez : 

```
Disk: wd0      geometry: 2610/255/63 [41943040 Sectors]
Offset: 0      Signature: 0xAA55
           Starting         Ending     LBA Info:
 #: id     C   H   S -      C   H  S [    start:     size ]
-----------------------------------------------------------------
 0: 00     0   0   0 -      0   0  0 [        0:        0 ] unused
 1: 00     0   0   0 -      0   0  0 [        0:        0 ] unused
 2: 00     0   0   0 -      0   0  0 [        0:        0 ] unused
*3: A6     0   1   2 -   2609 254 63 [       64: 41929586 ] OpenBSD
Use (W)hole disk MBR, whole disk (G)PT, (O)penBSD area or (E)dit? [W]
```

Nous voilà à la partie sans doute la plus complexe : le partitionnement. Notez
que celui par défaut proposé par l'installateur conviendra dans la
majorité des cas.

Je tape donc "W" (ou "G") dans la suite pour utiliser le disque entier, puis "A" pour le partitionnement automatique.

Vous pouvez modifier le partitionnement par défaut en tapant "E".

```
Setting OpenBSD MBR partition to whole wd0...done.
The auto-allocated layout for wd0 is :
#            size     offset  fstype [fsize bsize   cpg]
  a:   738.1M         64  4.2BSD   2048 16384     1 # /
  b:   223.8M    1511648  swap
  c: 20480.0M          0  unused
  d: 172.9.1M    1969888  4.2BSD   2048 16384     1 # /tmp
  e:  1791.0M    4372032  4.2BSD   2048 16384     1 # /var
  f:  1558.1M    8040032  4.2BSD   2048 16384     1 # /usr
  g:   906.8M   11230976  4.2BSD   2048 16384     1 # /usr/X11R6
  h:  3364.2M   13088192  4.2BSD   2048 16384     1 # /usr/local
  i:  1287.2M   19977984  4.2BSD   2048 16384     1 # /usr/src
  j:  1826.5M   22614208  4.2BSD   2048 16384     1 # /usr/obj
  k:  7604.8M   26354784  4.2BSD   2048 16384     1 # /home
Use (A)uto layout, (E)dit auto layout, or create (C)ustom layout? [a] e
```


Ensuite, vous pouvez lire le
[manuel de ``disklabel`` https://man.openbsd.org/OpenBSD-current/man8/disklabel.8]
si vous êtes perdus. Il faut en général indiquer une action
(avec une lettre) à
réaliser sur la partition où il faudra l'effectuer. 

Par exemple : 
la suite ``d f (entrée)`` permet de supprimer la partition f (delete f), qui était ``/usr``, et 
``d k`` pour supprimer le ``/home``. 

Ensuite, taper ``a f`` permet de re-créer cette
partition, et d'en définir la taille. Notez
que vous pouvez définir une taille en pourcentage du disque (par exemple ``50%``) ou
en pourcentage de l'espace libre restant (``50&``). Ici, je vais réduire le
``/home`` au profit de ``/usr`` qui prendra 75% de l'espace restant.
De la même façon avec ``a k``, on re-crée ``/home`` à la taille souhaitée.


```
Use (A)uto layout, (E)dit auto layout, or create (C)ustom layout? [a] e
Label editor (enter '?' for help at any prompt)
> d f
> d k
> a f
offset: [26354784]
size: [15574866] 75&
FS type: [4.2BSD]
mount point: [none] /usr
> a k
offset: [38035904]
size: [3893746]
FS type: [4.2BSD]
mount point: [none] /home
```



À tous moments, vous pouvez taper ``p`` pour afficher les partitions actuelles,
histoire de voir où vous en êtes.

```
> p 
OpenBSD area: 64-41929650; size: 41929586; free: 3190962
The auto-allocated layout for wd0 is :
#           size       offset  fstype [fsize bsize   cpg]
  a:     1511584           64  4.2BSD   2048 16384     1 # /
  b:      458240      1511648  swap
  c:    41943040            0  unused
  d:     2402144      1969888  4.2BSD   2048 16384     1 # /tmp
  e:     3668000      4372032  4.2BSD   2048 16384     1 # /var
  f:    11681120     26354784  4.2BSD   2048 16384     1 # /usr
  g:     1857216     11230976  4.2BSD   2048 16384     1 # /usr/X11R6
  h:     6889792     13088192  4.2BSD   2048 16384     1 # /usr/local
  i:     2636224     19977984  4.2BSD   2048 16384     1 # /usr/src
  j:     3740576     22614208  4.2BSD   2048 16384     1 # /usr/obj
  k:     3893728     38035904  4.2BSD   2048 16384     1 # /home
```


Quelques points à garder en tête : 
- La partition "``b``" est réservée au swap.
- La lettre "``c``" est réservée, elle représente tout le disque. 
- Vos sites web seront enregistrés dans ``/var``. Songez à lui attribuer une
  place suffisante.
- Vous pouvez définir des tailles en ajoutant une unité (``50G``), un
  pourcentage du disque (``5%``) ou un pourcentage de l'espace libre restant
  (``100&``).


Taper ``q`` permet de valider les changements.

```
> q
Write new label?: [y]
newfs: reduced number of fragments per cylinder group from 94472 to 94096 to
enlarge last cylinder group
/dev/rwd0a: 738.1MB in 1511584 sectors of 512 bytes
5 cylinder groups of 114.559MB, 7334 blocks, 14720 inodes each
...
...
/dev/wd0a (8c0364801ae0817e.a) on /mnt type ffs  \
    (rw, asynchronous,local, nodev, nosuid)
/dev/wd0k (8c0364801ae0817e.k) on /mnt/home type ffs  \
    (rw, asynchronous,local, nodev, nosuid)
...
...
```


	STOP ! Je n'ai aucune idée de l'espace à consacrer à mes partitions. Un coup
	de main svp ?


Si vous doutez, je vous propose la répartition suivante très simple : 

- 10G pour ``/usr/local`` seront amplement suffisants pour contenir les
  programmes installés.
- Entre 10G et 50G pour ``/var`` qui contiendra vos sites et les 
[journaux #logs]. Vous pouvez attribuer encore plus d'espace si vous pensez en
avoir besoin. 
- L'espace restant ira pour la partition racine et donc tout le reste : ``/``.



Enfin l'installation des composants du système peut commencer. L'installateur
doit savoir où aller les chercher : sur le cdrom d'installation (``cd0``), la
clé USB d'installation (``disk``) ou directement sur un miroir de téléchargement
(``http``). 
Choisissez
``cd0`` ou ``disk`` si vous installez avec une image install*.* ou bien ``http``
en cas de doute.

Petite astuce pour choisir un miroir si ça vous intéresse. Entrez "?" pour avoir
la liste numérotée des serveurs disponibles. Quittez cette liste avec "q", puis entrez seulement le numéro du
miroir que vous souhaitez utiliser.


Pour ajouter un
set, saisissez simplement son nom, par exemple ``gameOBSDMINVERSION.tgz``. Pour retirer un set,
saisissez son nom précédé du signe moins : ``-gameOBSDMINVERSION.tgz``. Ils ne
prennent vraiment pas beaucoup de place, je vous conseille de tout laisser
coché.

```
Let's install the sets!
Location of sets (cd0 disk http or 'done') [http]
HTTP proxy URL? (e.g. ’http://proxy:8080’, or ’none’) [none]
HTTP Server? (hostname, list#, ’done’ or ’?’) [ftp.fr.openbsd.org]
Server directory? [pub/OpenBSD/6.2/amd64]

Select sets by entering a set name, a file name pattern or ’all’. De-select
sets by prepending a ’-’ to the set name, file name pattern or ’all’. Selected
sets are labelled ’[X]’.

  [X] bsd     [X] baseOBSDMINVERSION.tgz  [X] gameOBSDMINVERSION.tgz    [X] xfontOBSDMINVERSION.tgz
  [X] bsd.rd  [X] compOBSDMINVERSION.tgz  [X] xbaseOBSDMINVERSION.tgz   [X] xservOBSDMINVERSION.tgz
  [ ] bsd.mp  [X] manOBSDMINVERSION.tgz   [X] xshareOBSDMINVERSION.tgz

Set name(s)? (or ’abort’ or ’done’) [done]
```


``bsd.mp`` est le noyau optimisé pour les systèmes multi-processeurs, et
``bsd`` le noyau classique. C'est l'un ou l'autre qui sont utilisé, et non pas
l'un en plus de l'autre. OpenBSD est suffisamment intelligent pour choisir le
noyau optimisé si le matériel le permet, vous n'avez rien à faire.

Si vous installez les sets à partir du CD, un avertissement sur la signature
apparaît. Rien d'inquiétant, vous pouvez dans ce cas continuer.

```
Directory does not contain SHA256.sig. Continue without verification? [no] yes
```


L'installation avance 

```
Get/Verify SHA256.sig    100% |**************************| 2152        00:00
Signature Verified
Get/Verify bsd           100% |**************************| 10423 KB    00:14
Get/Verify bsd.rd        100% |**************************| 9215  KB    00:11
...
...
Installing baseOBSDMINVERSION.tgz    100% |**************************| 52181 KB
...
...
Installing xservOBSDMINVERSION.tgz    100% |**************************| 22686 KB
Location of sets? (cd0 disk http or 'done' [done]
```


Pour terminer, l'installateur s'occupe de la configuration et de générer un
noyau unique :

```
Saving configuration files...done.
Making all device nodes...done.
Relinking to create a unique kernel...

CONGRATULATIONS! Your Openbsd install has been successfully completed!

When you login to your new system the first time, please read your mail using the 'mail' command.

Exit to (S)hell, (H)alt or (R)eboot? [reboot]
```


Et voilà, l'installation est terminée, on valide le ``reboot`` pour redémarrer.

Lors du premier démarrage, éditez (créez) le fichier ``/etc/installurl`` puis
assurez-vous d'avoir **une ligne** contenant le 
[miroir de téléchargement https://www.openbsd.org/ftp.html] choisi. Pour faire
simple, autant utiliser le CDN d'OpenBSD qui vous redirigera automatiquement
vers un serveur rapide : 


```
https://cdn.openbsd.org/pub/OpenBSD
```


Pour les personnes souhaitant chiffrer entièrement leur serveur (pour protéger
les données en cas de cambriolage par exemple), rendez-vous 
[un peu plus loin dans le manuel #diskcrypt].

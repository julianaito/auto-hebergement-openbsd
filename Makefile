N=auto-hebergement-openbsd
all:
	@python2 /home/xavier/bin/txt2tags.py -t html5 -o ${N}.html src/${N}.t2t 
	@sed -i "s;\.\.\.;…;g" ${N}.html
	@txt2tags -t xhtml -o ${N}.xhtml src/${N}.t2t 
	@sed -i "s;\.\.\.;…;g" ${N}.xhtml
entr:
	find src/ | entr -s 'make'
pdf:
	@txt2tags -t tex -o ${N}.tex src/${N}.t2t 
	@pdflatex ${N}.tex 
ebook:
	@txt2tags -t tex -o ${N}-ebook.tex src/${N}.t2t 
	@/usr/bin/sed -i "s;\\\lfoot{.*;\\\lfoot{};;" ${N}-ebook.tex
	@/usr/bin/sed -i "s;\\\usepackage{geometry}.*;;;" ${N}-ebook.tex
	@/usr/bin/sed -i "s;a5paper,twoside,10pt;a4paper,11pt;;" ${N}-ebook.tex
	@/usr/bin/sed -i "s;-gray.png;.png;g" ${N}-ebook.tex
	@/usr/bin/sed -i "s;-gray.jpg;.jpg;g" ${N}-ebook.tex
	@pdflatex ${N}-ebook.tex 
	@gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=${N}-ebook-tmp.pdf ${N}-ebook.pdf
	@pdf2djvu -j0 -o ${N}-ebook.djvu ${N}-ebook-tmp.pdf
	@rm ${N}-ebook-tmp.pdf
	@mkdir -p ${N}-ebook
	@mv ${N}-ebook.pdf ${N}-ebook
	@mv ${N}-ebook.djvu ${N}-ebook
	@tar cvzf ${N}-ebook.tgz ${N}-ebook
serve:
	sleep 1 && iridium --enable-unveil http://127.0.0.1:8000/geek/gitreps/${N}/${N}.html &
	cd $(DESTDIR) && python3 -m http.server 
install:
	cp ${N}.html ~/geek/site/epicededune/Rendez-vous_sur_Arrakis/ah/index.html
	cp ${N}.xhtml ~/geek/site/epicededune/Rendez-vous_sur_Arrakis/ah/index.xhtml
	cp style.css ~/geek/site/epicededune/Rendez-vous_sur_Arrakis/ah/
	rsync img/ ~/geek/site/epicededune/Rendez-vous_sur_Arrakis/ah/
